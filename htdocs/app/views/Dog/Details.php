<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<nav class="navbar navbar-icon-top navbar-expand-lg navbar-light bg-light" id="navMenu">
        <img class="navbar-brand" src="/images/logo.png" alt="PawScouts" height="150" width="180">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li class="nav-item active">
            <?php $profile_link = '/'.$_SESSION['role'].'/Details/'.$_SESSION['profile_id'].'';?> 
            <a class="nav-link" <?php echo "href='$profile_link'";?> >
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>

	<?php 
	$dog = $data['dog'];
	//var_dump($dog); 
  ?>
  <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">

                <div class="card-body">
                    <div class="card-title mb-4">
                        <div class="d-flex justify-content-start">
                            <div class="image-container">
                                <?php echo "<img src='/pictures/$dog->picture' id='imgProfile' alt='Dog image' style='width: 150px; height: 150px' class='img-thumbnail' />"; ?> <!-- it only works with php -->
                            </div>
                            <div class="userData ml-3">
                                <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold">
                                <?php
                                  echo "".$dog->name; 
                                 ?></h2>

                                <?php 
                                //request the dog
                                if($_SESSION['role']=='Person') {
                                    echo '<a href="/Request/Create/'.$dog->dog_id.'">Request this dog</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Basic Info</a>
                                </li>
                            </ul>
                            <div class="tab-content ml-1">
                                <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Name</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo "".$dog->name;  ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Age</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $dog->age; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Gender</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php 
                                              echo $dog->gender;
                                              ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Color</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $dog->color; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Size of the dog</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php $size = $this->model('Size')->findSize($dog->size_id);
                                                  echo $size->size;
                                            ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Breed</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php $mixbreeds = $this->model('MixBreed')->findByDogId($dog->dog_id);
                                                if($mixbreeds != null){
                                                    foreach($mixbreeds as $mixbreed) {
                                                        echo $mixbreed->name.'. ';
                                                    }
                                                }
                                                else
                                                    echo 'No breed found.';
                                            ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Is the dog trained?</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php 
                                            if($dog->isTrained==1)
                                              echo 'Yes'; 
                                            elseif($dog->isTrained==0)
                                              echo 'No'; 
                                            ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Experience level of the dog</label>
                                            
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $dog->experienceLevel; ?>
                                        </div> 
                                        <small>Legend: 1 is a lot of experience with dogs required. 10 is no previous experience with dogs required. 1 is usually for very agressive dogs.</small>
                                    </div>
                                    <hr />                          
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Additional Description</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $dog->description; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Shelter</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php  $shelter = $this->model('Shelter')->findShelter($dog->shelter_id);
                                                    echo "<a href='/Shelter/Details/$shelter->shelter_id'>$shelter->name</a>";
                                            ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <?php
                                    
                                        if ($_SESSION['profile_id'] == $dog->shelter_id)
                                        {
                                            echo "<a class='btn btn-primary' href='/Dog/Edit/$dog->dog_id'>Edit</a>      ";
                                            echo "<a class='btn btn-danger' href='/Dog/Delete/$dog->dog_id'>Delete Dog</a>";
                                            if (isset($data['confirmation']) && $data['confirmation'] != ''){
                                                echo "<hr />Are you sure you want to delete your dog?     ";
                                                echo '<a class="btn btn-success" href="/Dog/Delete/'.$dog->dog_id.'/Y">Yes</a>             ';
                                                echo '<a class="btn btn-danger" href="/Dog/Delete/'.$dog->dog_id.'/N">No</a>';
                                            }
                                        }
                                        
                                    ?>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
             </div>
            </div>
        </div>


	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>