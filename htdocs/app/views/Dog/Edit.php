<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	
	<?php 
	
	$dog = $data['dog'];
	//var_dump($dog); 
  	?>
	<div class="container" id="loginContainer">
		<h3 class="form-heading" style="text-align: center;">Edit your dog</h1>
		<div class="row main">
			<div class="main-login main-center">
				<form class="form-horizontal" method="post" <?php echo"action='/Dog/Edit/$dog->dog_id'"; ?> enctype="multipart/form-data">
					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="name" placeholder="Name..." <?php echo 'value="'.$dog->name.'"'; ?> />
							</div>
							<?php if(isset($data['ivalidfName'])){
									echo "<p class='invalid'>".$data['ivalidfName']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Age</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="age" placeholder="Age..." <?php echo 'value="'.$dog->age.'"'; ?>/>
							</div>
							<?php if(isset($data['invalidAge'])){
									echo "<p class='invalid'>".$data['invalidAge']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Gender</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<select name="gender" class="custom-select my-1 mr-sm-2">
										<option selected value="Male">Male</option>
										<option value="Female">Female</option>
								</select>
							</div>
							
						</div>
					</div>
					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Color</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="color" placeholder="Color..." <?php echo 'value="'.$dog->color.'"'; ?>/>
							</div>
							<?php if(isset($data['ivalidfName'])){
									echo "<p class='invalid'>".$data['ivalidfName']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Import Picture</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="file" class="form-control" name="picture" />
							</div>
							<?php if(isset($data['invalidPic'])){
									echo "<p class='invalid'>".$data['invalidPic']." </p>";
							}?>
						</div>
					</div>

					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Is the dog trained?</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<div class="radio-button" >
								  <label><input type="radio" name="isTrained" value="No" />  No</label>
								</div>
								<div class="radio-button">
								  <label><input type="radio" name="isTrained" value="Yes" checked />  Yes</label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Ease of handling</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="experienceLevel" placeholder="" <?php echo 'value="'.$dog->experienceLevel.'"'; ?> />
								<small>Legend: 1 is a lot of experience with dogs required. 10 is no previous experience with dogs required. 1 is usually for very agressive dogs.</small>
							</div>
							<?php if(isset($data['invalidLevel'])){
									echo "<p class='invalid'>".$data['invalidLevel']." </p>";
							}?>
						</div>
					</div>

					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Size</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<select name="size" class="custom-select my-1 mr-sm-2">
										<option selected value="XS">XS</option>
										<option value="S">S</option>
										<option value="M">M</option>
										<option value="L">L</option>
										<option value="XL">XL</option>
										<option value="XXL">XXL</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Breed</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<select  name="mixbreeds[]" size="10" style="height: 50%;" class="custom-select my-1 mr-sm-2" multiple>
										<?php
										$breeds = $this->model('Breed')->getAll();
										if(isset($breeds)) { 
											foreach($breeds as $breed) { 
												echo "<option value='$breed->breed_id'>$breed->name</option>";
											}
										}
										
										?>
								</select>
								<small>You can select multiple breeds by holding down the Ctrl key. Also, you can type a letter to go through the breeds that start with that letter.</small>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="text" class="cols-sm-2 control-label">Description</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="description" placeholder="More important information about the dog here..." <?php echo 'value="'.$dog->description.'"'; ?> />
							</div>
						</div>
					</div>

					<?php if(isset($data['another'])){
							echo "<p class='invalid'>".$data['another']." </p>";
					}?>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary btn-lg btn-block login-button" name="action" value="Update Dog"/>
					</div>
					<div class="form-group ">
						<a href="/Dog/Index/">Back To Your Dogs</a>
					</div>
					<?php if(isset($data['errormessage'])){
									echo "<p class='invalid'>".$data['errormessage']." </p>";
					}?>
				</form>
			</div>
		</div>
	</div>


	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>