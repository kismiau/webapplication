<html>
<head>

	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<nav class="navbar navbar-icon-top navbar-expand-lg navbar-light bg-light" id="navMenu">
        <img class="navbar-brand" src="/images/logo.png" alt="PawScouts" height="150" width="180">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li class="nav-item active">
            <?php $profile_link = '/'.$_SESSION['role'].'/Details/'.$_SESSION['profile_id'].'';?> 
            <a class="nav-link" <?php echo "href='$profile_link'";?> >
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
      </div>
      </nav>
      <?php if($_SESSION['role']=='Person') { ?>
     <form class="form-horizontal" method="post" action="/Dog/Search/">
	     <div class="form-group">
			<div class="col-sm-2">
				<div class="input-group">
					<input type="text" class="form-control" name="search" placeholder="Search" required/>
				</div>
				<div class="input-group">
					<select name="category" class="custom-select my-1 mr-sm-2">
							<option selected value="name">Name</option>
							<option selected value="age">Age</option>
							<option value="gender">Gender</option>
							<option value="color">Color</option>
							<option value="size">Size</option>
							<option value="breed">Breed</option>
							<option value="isTrained">Is the dog trained?</option>
							<option value="experienceLevel">Experience level required for the dog</option>
							<option value="description">Description</option>
					</select>
				</div>
			</div>
			</div>
			<div class="input-group ">
				<div class="col-sm-2">
					<input type="submit" class="btn btn-primary btn-lg btn-block login-button" name="action" value="Search"/>
				</div>
			</div>
		</form>
	<?php } 
		elseif($_SESSION['role']=='Shelter') {
	?>
	<form class="form-horizontal" method="post" action="/Dog/Order/" >
	     <div class="form-group">
			<div class="col-sm-2">
				<div class="input-group">
				</div>
				<div class="input-group">
					<select name="order" class="custom-select my-1 mr-sm-2">
							<option selected name="order" value="name">Name</option>
							<option name="order" value="age">Age</option>
							<option name="order" value="gender">Gender</option>
							<option name="order" value="color">Color</option>
							<option name="order" value="size_id">Size</option>
							<option name="order" value="isTrained">Is the dog trained?</option>
							<option name="order" value="experienceLevel">Experience level required for the dog</option>
							<option name="order" value="description">Description</option>
					</select>
				</div>
			</div>
			</div>
			<div class="input-group ">
				<div class="col-sm-2">
					<input type="submit" class="btn btn-primary btn-lg btn-block login-button" name="action" value="Order Dogs"/>
				</div>
			</div>
		</form>
	<?php }
		$dogs = $data['dogs'];
	 ?>
	<div class="container" id="loginContainer">
	<h1 class="form-heading" style="text-align: center;">DOGS</h1>
		<div class="login-form">
			<div class="main-div">
			    <div class="panel">
			    	<?php 
			    		if($_SESSION['role']=='Shelter') {
			    			echo '<p style="text-align: center;">Here is a list of your dogs</p>';
			    		}
			    		else if($_SESSION['role']=='Person') {
			    			echo '<p style="text-align: center;">Here is a list of all available dogs</p>';
			    		}
			    	?>
			   
			   </div>
				<?php
					
					if($_SESSION['role']=='Shelter') { 
						echo '<a href="/Dog/Create" class="">Add a dog</a><p></p>';
						if(sizeof($dogs)==0)
							echo '<p>You have no dogs in your collection. Add a dog to see them here.</p>';
					} 	

					$counter = 0;
					foreach ($dogs as $dog) {
						//if dog belongs to shelter, show this
						if($_SESSION['role']=='Shelter') { 
							if ( $dog->shelter_id==$_SESSION['profile_id']) { 
								if($counter % 2 == 0) {
									echo "<div class='card-deck'>";
								}
							echo "<div class='card' style='400px'>
									<img src='/pictures/$dog->picture' id='imgProfile' alt='Dog image' style='width: 150px; height: 150px' class='img-thumbnail' />
									<div class='card-body'>
										<h4 class='card-title'>$dog->name</h4>
										<p>Age: $dog->age</p>
										<p>Gender: $dog->gender</p>
										<a href='/Dog/Details/$dog->dog_id' class='btn btn-success'>Dog Details</a> ";
							//buttons	
							echo "<a href='/Dog/Edit/$dog->dog_id' class='btn btn-primary'>Edit this dog</a> ";
															
							echo	'</div></div>';
							if($counter % 2 == 0) {
								echo '</div>';
							}}
						}
						elseif($_SESSION['role']=='Person') {
							if($counter % 2 == 0) {
								echo "<div class='card-deck'>";
							}
							echo "<div class='card' style='400px'>
									<img src='/pictures/$dog->picture' id='imgProfile' alt='Dog image' style='width: 150px; height: 150px' class='img-thumbnail' />
									<div class='card-body'>
										<h4 class='card-title'>$dog->name</h4>
										<p>Age: $dog->age</p>
										<p>Gender: $dog->gender</p>
										<a href='/Dog/Details/$dog->dog_id' class='btn btn-success'>Dog Details</a> ";
									
							echo	'</div></div>';
							if($counter % 2 == 0) {
								echo '</div>';
							}
						}
						$counter ++;
					}
				?>
				</div>
			</div>
		</div>
	</div>

	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>

