<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<div class="container" id="createUser">
		<div class="row main">
			<div class="main-login main-center">
				<form class="form-horizontal" method="post" action="/User/signup/">
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Username</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
								<input type="text" class="form-control" name="username" placeholder="Enter your Email"/>
							</div>
							<?php if(isset($data['invalidEmail'])){
									echo "<p class='invalid'>".$data['invalidEmail']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
								<input type="password" class="form-control" name="password"  placeholder="Enter your Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required/>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
								<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>

							</div>
							<?php if(isset($data['notMatch'])){
									echo "<p class='invalid'>".$data['notMatch']." </p>";
								}?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Role</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<div class="radio-button" >
								  <label><input type="radio" name="role" value="Person" checked />  Person</label>
								</div>
								<div class="radio-button">
								  <label><input type="radio" name="role" value="Shelter" />  Shelter</label>
								</div>
							</div>
						</div>
					</div>
					<?php if(isset($data['another'])){
									echo "<p class='invalid'>".$data['another']." </p>";
					}?>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary btn-lg btn-block login-button" name="action" value="Register"/>
					</div>
					<div class="form-group ">
						<a href="/User/Login/">Back To Login</a>
					</div>
					<?php if(isset($data['errormessage'])){
									echo "<p class='invalid'>".$data['errormessage']." </p>";
					}?>
				</form>
			</div>
		</div>
	</div>

	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>