<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

<!-- There is a error message if somethign happens, its in data['error'] -->
<div class="container" id="loginContainer">
	<h1 class="form-heading" style="text-align: center;">LOGIN</h1>
		<div class="login-form">
			<div class="main-div">
			    <div class="panel">
			   <p style="text-align: center;">Please enter your email and password</p>
			   </div>
			    <form class="form-horizontal" method="post" action="/User/login/">

			        <div class="form-group">
			            <input type="email" class="form-control" name="username" placeholder="Username...">
			        </div>

			        <div class="form-group">
			            <input type="password" class="form-control" name="password" placeholder="Password...">
			        </div>
			        <?php if(isset($data['invalidPassword'])){
										echo "<p class='invalid'>".$data['invalidPassword']." </p>";
					}?>
					<?php if(isset($data['error'])){
										echo "<p class='invalid'>".$data['error']." </p>";
					}?>
			        <div class="form-group">
			        <a href="/User/Create/">Create User</a>
			    	</div>
			        
			        <input type="submit" class="btn btn-primary" name="action" value="Login"/>
			    </form>
			</div>
		</div>
	</div>
</div>
	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>