<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <nav class="navbar navbar-icon-top navbar-expand-lg navbar-light bg-light" id="navMenu">
        <img class="navbar-brand" src="/images/logo.png" alt="PawScouts" height="150" width="180">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/<?php echo $_SESSION['role'];?>/Details/">
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>

	<?php
      $requests = $data['requests'];
		
	?>
    <div class="row">
        <div class="requests">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-list" aria-hidden="true"></i> Requests
                </div>
                <div class="panel-body" style="width: 900px;">
                    <ul class="list-group">
                    	<?php
                        if (sizeof($requests) < 1){
                          echo '<li class="list-group-item" >
                                        <div class="checkbox">
                                            <label> You have no requests</label>
                                        </div></li>';
                        }
                        else{


                    		foreach ($requests as $request){
                    			$dog = $this->model('Dog')->find($request->dog_id);
            							$shelter = $this->model('Shelter')->findShelter($dog->shelter_id);
            							$person = $this->model('Person')->findPerson($request->person_id);
            							$role = $_SESSION['role'];
            							if ($role == 'Shelter'){
            								echo '<li class="list-group-item" >
        		                            <div class="checkbox">
        		                                <label>'.
        		                                    $person->f_name.' '.$person->l_name.'  &  '.$dog->name
        		                                .'</label>
        		                            </div>';
                                        /**
                                        A shelter can look at the details of dog or person of incoming request.
                                        */
        		                            if ($request->status == 'Pending'){
        		                            	echo '<div class="pull-right action-buttons">
        		                            	<a href="/Person/Details/'.$person->person_id.'"> See Person</a>
        		                            	<a href="/Dog/Details/'.$dog->dog_id.'" class="see"> See Dog</a>
                                          <p>
                                          <a href="/Request/EditStatus/'.$request->request_id.'/Accepted" id="accept"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Accept</a>
                                          <a href="/Request/EditStatus/'.$request->request_id.'/Denied"  id="reject"><i class="fa fa-ban" aria-hidden="true"></i> Reject</a>
                                          </p>
        		                            	</div>';
        		                            }
                                        else{
                                          echo '<div class="pull-right action-buttons">
                                          <a href="/Person/Details/'.$person->person_id.'"> See Person</a>
                                          <a href="/Dog/Details/'.$dog->dog_id.'" class="see"> See Dog</a>';
                                          if ($request->status == 'Accepted')
                                            echo'<p style="color:green;"> '.$request->status.'</p>';
                                          else
                                            echo'<p style="color:red;"> '.$request->status.'</p>';
                                          echo '</div>';
                                        }
        		                        	echo '</li>';
            							}
            							else{
            								echo '<li class="list-group-item">
        		                            <div class="checkbox">
        		                                <label>'.
        		                                    $shelter->name.'  &  '.$dog->name
        		                                .'</label>';
                                            if ($request->status == 'Accepted')
                                              echo '<a  style="float:right;" href="/Appointment/Create/'.$request->dog_id.'" class="btn btn-warning"> Set Appointment</a>';
        		                            echo '</div>';
        		                            	echo '<div class="pull-right action-buttons" style="clear:both;float:right;margin-top:5px;">
        		                            	<a href="/Shelter/Details/'.$shelter->shelter_id.'" > See Shelter</a>
        		                            	<a href="/Dog/Details/'.$dog->dog_id.'" class="see"> See Dog</a>';
                                          if ($request->status == 'Accepted')
                                            echo'<p style="color:green;"> '.$request->status.'</p>';
                                          else
                                            echo'<p style="color:red;"> '.$request->status.'</p>';
        		                              echo '<a href="/Request/Delete/'.$request->request_id.'" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete Request</a>
        		                            	</div>';
        		                        	echo '</li>';
            							}
	                    			
                    		}
                      }
	                        
                        ?>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>



	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>
          