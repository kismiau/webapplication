<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<div class="container" id="createPerson">
		<p style="text-align: center; color: purple;"> Create Profile</p>
		<div class="row main">
			<div class="main-login main-center">
				<form class="form-horizontal" method="post" action="/Person/Create/" enctype="multipart/form-data">
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">First Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="f_name" placeholder="First name..." required/>
							</div>
							<?php if(isset($data['ivalidfName'])){
									echo "<p class='invalid'>".$data['ivalidfName']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Last Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="l_name" placeholder="Last name..." required/>
							</div>
							<?php if(isset($data['ivalidlName'])){
									echo "<p class='invalid'>".$data['ivalidlName']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Phone Number</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="phone" placeholder="(111) 111-1111" required/>
							</div>
							<?php if(isset($data['invalidPhoneNo'])){
									echo "<p class='invalid'>".$data['invalidPhoneNo']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Import Picture</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="file" class="form-control" name="picture" required/>
							</div>
							<?php if(isset($data['invalidPic'])){
									echo "<p class='invalid'>".$data['invalidPic']." </p>";
							}?>
						</div>
					</div>

					<?php if(isset($data['another'])){
									echo "<p class='invalid'>".$data['another']." </p>";
					}?>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary btn-lg btn-block login-button" name="action" value="Create Profile"/>
					</div>
					<div class="form-group ">
						<a href="/User/Login/">Back To Login</a>
					</div>
					<?php if(isset($data['errormessage'])){
									echo "<p class='invalid'>".$data['errormessage']." </p>";
					}?>
				</form>
			</div>
		</div>
	</div>
	
	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>