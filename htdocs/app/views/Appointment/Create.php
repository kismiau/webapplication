<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" >
<!-- <link href="/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
  
    <script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script> 
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">
  

  <!-- Compiled and minified JavaScript -->
  <!--script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script-->
</head>
<body>
	<nav class="navbar navbar-default" id="navMenu">
        <ul class="nav navbar-nav">
          <li>
        <img src="/images/logo.png" alt="PawScouts" height="130" width="180">
      </li>
        
          <li>
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li>
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li>
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li>
            <a class="nav-link" href="/<?php echo $_SESSION['role'];?>/Details/">
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li>
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
    </nav>
<?php 
  $dog_id = $data['dog_id'];
?>
<div class="container" id="createAppointment">
    <p style="text-align: center; color: purple;"> Create Appointment</p>
    <div class="row main">
      <div class="main-login main-center">
        <form class="form-horizontal" method="post" <?php echo 'action="/Appointment/Create/'.$dog_id.'"'?> enctype="multipart/form-data">
          <div class="form-group" id="address">
            <label for="address" class="cols-sm-2 control-label">Address</label>
            <div class="cols-sm-10">
              <div class="input-group">
                <input type="text" class="form-control" name="address" placeholder="1111 St street" required/>
              </div>
            </div>
          </div>
          <div class="form-group" id="comment" style="width: 850px;">
              <label for="comment">Comment:</label>
              <textarea class="form-control" rows="5" name="comment"></textarea>
          </div>
        
      </div>
    </div>
    <div class="row">
        <div class='col-sm-6'>
            <div class="form-group">
              <label for="address" class="cols-sm-2 control-label">Date and Time</label>
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' name="datetime" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </div>
    <div class="row">
        <div class='col-sm-6'>
          <div class="form-group ">
            <input type="submit" class="btn btn-primary login-button" name="action" value="Set Appointment" style="width: 150px;" />
          </div>
          <?php if(isset($data['error'])){
                  echo "<p class='invalid'>".$data['error']." </p>";
              }?>
        </div>
      </div>
      </form>
  </div>



    <script src="/js/popper.min.js" type="text/javascript"></script>
	<!--script src="/js/bootstrap.min.js" type="text/javascript"></script-->
</body>
<footer>
</footer>
</html>