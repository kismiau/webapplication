<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<nav class="navbar navbar-icon-top navbar-expand-lg navbar-light bg-light" id="navMenu">
        <img class="navbar-brand" src="/images/logo.png" alt="PawScouts" height="150" width="180">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/<?php echo $_SESSION['role']; ?>/Details/">
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>


    <?php
    	$appointments = $data['appointments'];
    ?>

    <div class="container" id="listAppointments">
    	<?php
    		foreach ($appointments as $appointment) {
    			$datetime = new DateTime($appointment->date_time);
    			$dog = $this->model('Dog')->find($appointment->dog_id);
    			$shelter = $this->model('Shelter')->findShelter($appointment->shelter_id);
    			$person = $this->model('Person')->findPerson($appointment->person_id);
    			if ($_SESSION['role'] == 'Person'){
    				$id = $shelter->shelter_id;
    				$role = 'Shelter';
    				$compagnon = $shelter->name;
    			}
    			else{
    				$id = $person->person_id;
    				$role = 'Person';
    				$compagnon = $person->f_name.' '.$person->l_name;
    			}
    			echo '	<div class="row row-striped appointment">
							<div class="col-2 text-right">
								<h1 class="display-4"><span class="badge badge-secondary">'.$datetime->format('d').'</span></h1>
								<h2>'.$datetime->format('M').'</h2>
							</div>
							<div class="col-10">
								<h3 class="text-uppercase"><strong>'
									.$dog->name.' - '.$compagnon .
								'</strong></h3>
								<ul class="list-inline">
								    <li class="list-inline-item"><i class="fa fa-calendar-o" aria-hidden="true"></i> '.$datetime->format('l').'</li>
									<li class="list-inline-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 
									'.$datetime->format('h').'h'.$datetime->format('i').' '.$datetime->format('A').'
									</li>
									<li class="list-inline-item"><i class="fa fa-location-arrow" aria-hidden="true"></i> '.$appointment->address.'</li>
								</ul>
								<p>'.$appointment->comment.'</p>
								<a class="btn btn-info" href="/Dog/Details/"'.$dog->dog_id.'>See Dog</a>
								<a class="btn btn-info" href="/'.$role.'/Details/'.$id.'">See '.$role.'</a> ';
								if ($_SESSION['role'] == 'Shelter')
								{
									echo ' <a class="btn btn-warning" href="/Appointment/Edit/'.$appointment->appointment_id.'"> Reschedule </a>';	
								}
				echo '
							</div>
						</div>';
	    			
    		}
		

		?>
		
	</div>




	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>