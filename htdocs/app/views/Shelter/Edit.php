<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<nav class="navbar navbar-icon-top navbar-expand-lg navbar-light bg-light" id="navMenu">
        <img class="navbar-brand" src="/images/logo.png" alt="PawScouts" height="150" width="180">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/Shelter/Details/">
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>
	<?php 
		$shelter = $this->model('Shelter')->findShelter($_SESSION['profile_id']);
	?>
	<div class="container" id="editPerson">
		<p style="text-align: center; color: purple;"> Edit Profile</p>
		<div class="row main">
			<div class="main-login main-center">
				<form class="form-horizontal" method="post" action="/Shelter/Edit/" enctype="multipart/form-data">
					<div class="form-group">
						<label class="cols-sm-2 control-label">Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="name" placeholder="<?php echo $shelter->name;?>"/>
							</div>
							<?php if(isset($data['ivalidName'])){
									echo "<p class='invalid'>".$data['ivalidName']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label class="cols-sm-2 control-label">Address</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="address" placeholder="<?php echo $shelter->address;?>"/>
							</div>
							<?php if(isset($data['ivalidAddress'])){
									echo "<p class='invalid'>".$data['ivalidAddress']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label class="cols-sm-2 control-label">Phone Number</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="phone" placeholder="<?php echo $shelter->phone;?>"/>
							</div>
							<?php if(isset($data['invalidPhoneNo'])){
									echo "<p class='invalid'>".$data['invalidPhoneNo']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label class="cols-sm-2 control-label">Website</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" name="website" placeholder="<?php echo $shelter->website;?>"/>
							</div>
							<?php if(isset($data['ivalidWebsite'])){
									echo "<p class='invalid'>".$data['ivalidWebsite']." </p>";
							}?>
						</div>
					</div>
					<div class="form-group">
						<label class="cols-sm-2 control-label">Import Picture</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<input type="file" class="form-control" name="picture"/>
							</div>
							<?php if(isset($data['invalidPic'])){
									echo "<p class='invalid'>".$data['invalidPic']." </p>";
							}?>
						</div>
					</div>

					<?php if(isset($data['another'])){
									echo "<p class='invalid'>".$data['another']." </p>";
					}?>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary btn-lg btn-block login-button" name="action" value="Submit Modifications"/>
					</div>
					<div class="form-group ">
						<a href="/Shelter/Details/0">Back To Details</a>
					</div>
					<?php if(isset($data['errormessage'])){
									echo "<p class='invalid'>".$data['errormessage']." </p>";
					}?>
				</form>
			</div>
		</div>
	</div>
	
	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
