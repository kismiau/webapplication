<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
     <nav class="navbar navbar-icon-top navbar-expand-lg navbar-light bg-light" id="navMenu">
        <img class="navbar-brand" src="/images/logo.png" alt="PawScouts" height="150" width="180">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="/Dog/Index">
              <i class="fa fa-home"></i>
              Home
              </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Request/Index/">
              <i class="fa fa-envelope-o">
              </i>
              Requests
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/Appointment/Index/">
              <i class="menu-icon fa fa-phone"></i>
              Appointments
            </a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/Shelter/Details/">
              <i class="fa fa-user"></i>
              Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/User/Logout/">
              <i class="fa fa-sign-out"></i>
              Logout
            </a>
          </li>
        </ul>
      </div>
    </nav>
	<?php
		$shelter = $data['profile'];
	?>

	<div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">

                <div class="card-body">
                    <div class="card-title mb-4">
                        <div class="d-flex justify-content-start">
                            <div class="image-container">
                                <img src="/pictures/<?php echo $shelter->picture; ?>" id="imgProfile" style="width: 150px; height: 150px" class="img-thumbnail" />
                            </div>
                            <div class="userData ml-3">
                                <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold">
                                <?php
                                	echo $shelter->name; 
                                 ?></h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Basic Info</a>
                                </li>
                            </ul>
                            <div class="tab-content ml-1">
                                <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Name</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $shelter->name; ?>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Address</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $shelter->address; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Phone</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo $shelter->phone; ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Website</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                        	<a href="<?php echo $shelter->website; ?>">
                                            <?php echo $shelter->website; ?></a>
                                        </div>
                                    </div>
                                    <hr />
                                    <?php
                                        if ($_SESSION['user_id'] == $shelter->user_id)
                                        {
                                            echo '<a class="btn btn-primary" href="/Shelter/Edit/">Edit</a>      ';
                                            echo '<a class="btn btn-danger" href="/Shelter/Delete/">Delete User</a>';
                                            if (isset($data['confirmation']) && $data['confirmation'] != ''){
                                                echo "<hr />Are you sure you want to delete your user?     ";
                                                echo '<a class="btn btn-success" href="/Shelter/Delete/Y">Yes</a>             ';
                                                echo '<a class="btn btn-danger" href="/Shelter/Delete/N">No</a>';
                                            }
                                        } 
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
            </div>
        </div>
    </div>
	<script src="/js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/js/popper.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>
</body>
<footer>
</footer>
</html>