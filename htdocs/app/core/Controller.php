<?php
	# Global class for all Controllers
	class Controller extends ModelCaller {
		# Calls the view that is passed as parameter
		protected function view($view, $data = []) {
			if(file_exists('app/views/' . $view . '.php') ) {
				require 'app/views/' . $view . '.php';
			}
			else {
				echo "Can't load view $view: file not found!";
			}
		}
	}
?>