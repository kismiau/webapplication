<?php 
	# Class that enables calling of models
	class ModelCaller { //all of the models should extend ModelCaller
		# Calls the model class that is passed as parameter
		public static function model($model) {
			if(file_exists('app/models/' . $model . '.php') ) {
				require_once 'app/models/' . $model . '.php';
				return new $model();
			}
			else
				return null;
		}
	}
?>