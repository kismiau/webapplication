<?php
	# Class that establishes the connection to the database
	class Model extends ModelCaller {
		static $_connection;
		# Connects to the database
		public function __construct() {
			$server = 'localhost';
			$DBName = 'pawscouts';
			$user = 'root';
			$pass = '';
			
			self::$_connection = new PDO("mysql:host=$server;dbname=$DBName;charset=utf8", $user, $pass);
			self ::$_connection->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
	}
?>