<?php
	# Class to communicate information from the ShelterController to the database itself.
	class Shelter extends Model {
		var $shelter_id;
		var $name;
		var $address;
		var $phone;
		var $website;
		var $picture;
		var $user_id; 
		
		# Default constructor
		public function __contruct() {
			parent::__construct(); 
		}

		
		# A user can register as a new shelter.
		public function insert(){
			$sql = "INSERT INTO Shelter (name,address,phone,website,picture,user_id) VALUES(:name,:address,:phone,:website,:picture,:user_id)";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['name'=>$this->name,'address'=>$this->address,
				'phone'=>$this->phone,'website'=>$this->website,'picture'=>$this->picture,'user_id'=>$_SESSION['user_id']]);
			$this->shelter_id = self::$_connection->lastInsertId();
		}

		# Returns requests according to a shelter
		public function findRequests(){
			$requests = $this->model('Request');
			return $requests->getShelterRequests($_SESSION['profile_id']);
		}

		# Returns appointments according to a shelter
		public function findAppointments(){
			$appointments = $this->model('Appointment');
			return $appointments->getShelterAppointments($_SESSION['profile_id']);
		}

		# Returns a shelter from a shelter_id as a parameter
		public function findShelter($user){
			$sql = "SELECT * FROM Shelter WHERE shelter_id =:shelter_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['shelter_id'=>$user]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Shelter");
			return $sth->fetch(); 
		}

		# As a shelter, I can see my profile.
		public function findProfile($user_id){
			$sql = "SELECT * FROM Shelter WHERE user_id =:user_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['user_id'=>$user_id]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Shelter");
			return $sth->fetch(); 
		}

		# As a shelter, I can modify my profile.
		public function update(){
			$sql = "UPDATE Shelter 
			SET name = :name, address = :address, phone=:phone, website=:website,picture=:picture WHERE shelter_id=:shelter_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['name'=>$this->name,'address'=>$this->address,
				'phone'=>$this->phone,'website'=>$this->website,'picture'=>$this->picture, 'shelter_id'=>$this->shelter_id]);
		}

		# As a shelter, I can delete my profile and all my dogs.
		public function delete(){
			$appointments = $this->model('Appointment')->getShelterAppointments($_SESSION['profile_id']);
			if (sizeof($appointments) > 0){	
				return false;
			}
			$this->model('Dog')->deleteAllForShelter($this->shelter_id);
			$this->deleteRequests();
			$user = $this->model('User')->getUser($this->user_id);
			$sql = "DELETE FROM Shelter WHERE shelter_id = :shelter_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['shelter_id'=>$this->shelter_id]);
			$user->delete();
			return true;
		}

		# Deletes requests associated to the shelter_id
		private function deleteRequests(){
			$sql = "DELETE FROM Request WHERE dog_id in (
			SELECT dog_id FROM Dog WHERE shelter_id = :shelter_id)";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['shelter_id'=>$this->shelter_id]);
		}
	}
?>