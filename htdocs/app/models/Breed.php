<?php
	# Class to communicate information from the DogController to the database itself.
	class Breed extends Model {
		public $breed_id;
		public $name;
		
		public function __contruct() {
			parent::__construct(); 
		}
		
		# Returns a breed found using the breed_id as a parameter
		public function findOneBreed($breed_id) {
			$sql = "SELECT * FROM Breed WHERE breed_id=:breed_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['breed_id'=>$breed_id]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Breed");
			return $stmt->fetch();
		}

		# Returns the breed_id of a breed using the name value as a parameter
		public function findBreedId($name) {
			$sql = "SELECT breed_id FROM Breed WHERE name=:name";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['name'=>$name]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Breed");
			return $stmt->fetch();
		}

		# Returns the name of a breed using the breed_id as a parameter
		public function findBreedName($breed_id) {
			$sql = "SELECT name FROM Breed WHERE breed_id=:breed_ids";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['breed_id'=>$breed_id]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Breed");
			return $stmt->fetch();
		}
		
		# Returns all breeds from the Breed table
		public function getAll() {
			$sql = "SELECT * FROM Breed ORDER BY name";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute();
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Breed");
			return $stmt->fetchAll();
		}
	}
?>