<?php
	# Class to communicate information from the PersonController to the database itself.
	class Person extends Model {
		public $person_id;
		public $f_name;
		public $l_name;
		public $phone;
		public $picture;
		public $user_id; 
		
		# Default constructor
		public function __contruct() {
			parent::__construct(); 
		}

		
		# A user can register as a new person.
		public function insert(){
			$sql = "INSERT INTO Person (f_name,l_name,phone,picture,user_id) VALUES(:f_name,:l_name,:phone,:picture,:user_id)";			
			$user_id = $_SESSION['user_id'];
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['f_name'=>$this->f_name,'l_name'=>$this->l_name,
				'phone'=>$this->phone,'picture'=>$this->picture,'user_id'=>$_SESSION['user_id']]);
			$this->person_id = self::$_connection->lastInsertId();
		}

		# Returns requests for the person's id
		public function findRequests(){
			$requests = $this->model('Request');
			return $requests->getPersonRequests($_SESSION['profile_id']);
		}

		# Returns appointments for the person's id
		public function findAppointments(){
			$appointments = $this->model('Appointment');
			return $appointments->getPersonAppointments($_SESSION['profile_id']);
		}

		# Returns the user information for a person
		public function findPerson($user){
			$sql = "SELECT * FROM Person WHERE person_id =:person_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['person_id'=>$user]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Person");
			return $sth->fetch(); 
		}

		# A person can see their profile.
		public function findProfile($user_id){
			$sql = "SELECT * FROM Person WHERE user_id =:user_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['user_id'=>$user_id]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Person");
			return $sth->fetch(); 
		}

		
		# As a person, I can modify my profile.
		public function update(){
			$sql = "UPDATE Person 
			SET f_name=:f_name, l_name=:l_name, phone=:phone, picture=:picture WHERE person_id=:person_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['f_name'=>$this->f_name,'l_name'=>$this->l_name,'phone'=>$this->phone,'picture'=>$this->picture, 'person_id'=>$this->person_id]);
		}

		# As a person, I can delete my profile.
		public function delete(){
			$appointments = $this->model('Appointment')->getPersonAppointments($_SESSION['profile_id']);
			if (sizeof($appointments) > 0){	
				return false;
			}
			$this->deleteRequests();
			$user = $this->model('User')->getUser($this->user_id);
			$sql = "DELETE FROM Person WHERE person_id = :person_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['person_id'=>$this->person_id]);
			$user->delete();
			return true;
		}

		# Deletes requests according to a person's id
		private function deleteRequests(){
			$sql = "DELETE FROM Request WHERE person_id = :person ";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['person'=>$this->person_id]);
		}
	}
?>