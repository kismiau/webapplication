<?php
	# Class to communicate information from the DogController to the database itself.
	class Size extends Model {
		public $size_id;
		public $size;
		
		# Default controller
		public function __contruct() {
			parent::__construct(); 
		}
		
		# Returns the size_id using the size field as a parameter
		public function findSizeId($size) {
			$sql = "SELECT size_id FROM Size WHERE size=:size";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['size'=>$size]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Size");
			return $stmt->fetch()->size_id;
		}

		# Returns the size using the size_id as a parameter
		public function findSize($size_id) {
			$sql = "SELECT * FROM Size WHERE size_id=:size_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['size_id'=>$size_id]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Size");
			return $stmt->fetch();
		}		
	}
?>