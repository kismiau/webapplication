<?php
	# Class to communicate information from the DogController to the database itself.
	class Dog extends Model {
		public $dog_id;
		public $name;
		public $age;
		public $gender;
		public $color;
		public $picture;
		public $isTrained;
		public $experienceLevel;
		public $description;
		public $size_id;
		public $breed_id;
		public $shelter_id;
		
		# Default constructor
		public function __contruct() {
			parent::__construct(); 
		}
		
		
		# A shelter can add a dog to their collection.
		public function insert() {
			$sql = "INSERT INTO Dog (name, age, gender, color, picture, isTrained, experienceLevel, description, size_id, shelter_id) VALUES (:name, :age, :gender, :color, :picture, :isTrained, :experienceLevel, :description, :size_id, :shelter_id)";
			$sth = self::$_connection->prepare($sql);
			
			$sth->execute(['name'=>$this->name, 'age'=>$this->age, 'gender'=>$this->gender, 'color'=>$this->color, 'picture'=>$this->picture, 'isTrained'=>$this->isTrained, 'experienceLevel'=>$this->experienceLevel, 'description'=>$this->description, 'size_id'=>$this->size_id, 'shelter_id'=>$this->shelter_id]);
			$dog_id = self::$_connection->lastInsertId();
			return $dog_id;
		}
		
		
		# A shelter can see a list of their dogs.
		# A user can see a list of all dogs.
		public function getAllDogs() {
			$sql = "SELECT * FROM Dog";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute();
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		
		# A shelter can order his dogs by characteristics.
		public function getAllOrderedDogs($input) {
			$sql = "SELECT * FROM Dog ORDER BY $input ASC"; //orderby uses $ for variable intead of :
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(); 
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}
		
		
		# A person can see details of a selected dog.
		# As a shelter, I can see details of a selected dog.
		public function find($dog_id) {
			$sql = "SELECT * FROM Dog WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$dog_id]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetch();
		}

		
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchName($input) {
			$sql = "SELECT * FROM Dog WHERE name LIKE :input";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchAge($input) {
			$sql = "SELECT * FROM Dog WHERE age LIKE :input"; 
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchGender($input) {
			$sql = "SELECT * FROM Dog WHERE gender LIKE :input";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

	
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchColor($input) {
			$sql = "SELECT * FROM Dog WHERE color LIKE :input";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		# A person can search for dogs by different characteristics and display these dogs.
		public function searchSize($input) {
			$sql = "SELECT * FROM Dog WHERE size_id in
						(select size_id from Size where size LIKE :input)";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchBreed($input) {
			$sql = "SELECT * FROM Dog WHERE dog_id in 
						(select dog_id from MixBreed, Breed where name LIKE :input)";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchIsTrained($input) {
			if($input=='Yes' || $input=='yes' || $input=='y')
				$input = true;
			elseif($input=='No' || $input=='no' || $input=='n')
				$input = false;

			$sql = "SELECT * FROM Dog WHERE isTrained LIKE :input";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

	
		# A person can search for dogs by different characteristics and display these dogs.
		public function searchExperienceLevel($input) {
			$sql = "SELECT * FROM Dog WHERE experienceLevel LIKE :input";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}

		# A person can search for dogs by different characteristics and display these dogs.
		public function searchDescription($input) {
			$sql = "SELECT * FROM Dog WHERE description LIKE :input";
			$stmt = self::$_connection->prepare($sql);
			$input = "%$input%";
			$stmt->execute(['input'=>$input]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			return $stmt->fetchAll();
		}
		
		# A shelter can remove a dog from their collection.
		public function deleteOne() {
			$sql = "DELETE FROM Appointment WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$this->dog_id]);

			$sql = "DELETE FROM Request WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$this->dog_id]);

			$sql = "DELETE FROM MixBreed WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$this->dog_id]);

			$sql = "DELETE FROM Dog WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$this->dog_id]);
			return true;
		}
		
		# Deletes all dogs for a specified shelter
		public function deleteAllForShelter($shelter_id){
			$sql = "SELECT * FROM Dog WHERE shelter_id =:shelter_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['shelter_id'=>$shelter_id]);
			$stmt->setFetchMode(PDO::FETCH_CLASS, "Dog");
			$dogs = $stmt->fetchAll();
			foreach ($dogs as $dog) {
				$dog->deleteOne();
			}
		}
		
		# A shelter can edit a dog’s characteristics.
		public function edit() {
			$sql = "UPDATE Dog SET name=:name, age=:age, gender=:gender, color=:color, picture=:picture, isTrained=:isTrained, experienceLevel=:experienceLevel, description=:description, size_id=:size_id,  shelter_id=:shelter_id WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$this->dog_id, 'name'=>$this->name, 'age'=>$this->age, 'gender'=>$this->gender, 'color'=>$this->color, 'picture'=>$this->picture, 'isTrained'=>$this->isTrained, 'experienceLevel'=>$this->experienceLevel, 'description'=>$this->description, 'size_id'=>$this->size_id, 'shelter_id'=>$this->shelter_id]);
		}
	}
?>