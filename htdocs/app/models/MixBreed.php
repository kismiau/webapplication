<?php
	# Class to communicate information from the DogController to the database itself.
	class MixBreed extends Model {
		public $mixbreed_id;
		public $breed_id;
		public $dog_id;
		
		# Default controller
		public function __contruct() {
			parent::__construct(); 
		}

		# Inserts a breed for a dog into the database
		public function insert() {
			$sql = "INSERT INTO MixBreed (breed_id, dog_id) VALUES (:breed_id, :dog_id)";
			$sth = self::$_connection->prepare($sql);

			$sth->execute(['breed_id'=>$this->breed_id, 'dog_id'=>$this->dog_id]); 
		}
		
		# Returns a breed found using the dog_id as a parameter
		public function findByDogId($dog_id) {
			$sql = "SELECT * FROM MixBreed, Breed WHERE MixBreed.breed_id = Breed.breed_id AND dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$dog_id]);
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "MixBreed");
			return $stmt->fetchAll();
		}
		
		# Gets all breeds from the MixBreed table
		public function getAll() {
			$sql = "SELECT * FROM MixBreed";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute();
			
			$stmt->setFetchMode(PDO::FETCH_CLASS, "MixBreed");
			return $stmt->fetchAll();
		}

		# Deletes a breed using a dog_id associated to it
		public function deleteOne($dog_id) {
			$sql = "DELETE FROM MixBreed WHERE dog_id=:dog_id";
			$stmt = self::$_connection->prepare($sql);
			$stmt->execute(['dog_id'=>$dog_id]);
			return true;
		}
	}
?>