<?php
	# Class to communicate information from the UserController to the database itself.
	class User extends Model {
		var $username;
		var $password;
		var $role;
		var $user_id;

		# Default constructor
		public function __contruct() {
			parent::__construct();
		}

		# A user can register as a new person.
		# A user can register as a new shelter.
		public function insert(){
			$sql = "INSERT INTO User (username,password,role) VALUES(:username,:password,:role)";
			$sth = self::$_connection->prepare($sql);
			$sth->execute([
				'username' => $this->username,
				'password' => $this->password,
				'role' => $this->role]);
		}

		# A user can login as a person.
		# A user can login as a shelter.
		# Returns a user according to their username
		public function findUser( $username){
			$sql = "SELECT * FROM User WHERE username = :username";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['username'=>$username]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "User");
			return $sth->fetch();
		}

		# Returns a user according to their user_id
		public function getUser( $userId){
			$sql = "SELECT * FROM User WHERE user_id = :userId";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['userId'=>$userId]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "User");
			return $sth->fetch();
		}

		# Deletes a user according to their user_id
		public function delete(){
			$sql = "DELETE FROM User WHERE user_id = :user_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['user_id'=>$this->user_id]);
		}

	}
?>