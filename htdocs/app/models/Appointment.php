<?php
	# The Appointment Model class
	class Appointment extends Model {
		public $appointment_id;
		public $date_time;
		public $address;
		public $comment;
		public $shelter_id;
		public $person_id;
		public $dog_id;
		
		# Default constructor
		public function __contruct() {
			parent::__construct(); 
		}

		
		# As a person, I can set an appointment.
		public function insert(){
			$sql = "INSERT INTO Appointment (date_time,address,comment,shelter_id,person_id,dog_id) VALUES(:date_time,:address,:comment,:shelter_id,:person_id,:dog_id)";	
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['date_time'=>$this->date_time,'address'=>$this->address,
				'comment'=>$this->comment,'shelter_id'=>$this->shelter_id,'person_id'=>$this->person_id,'dog_id'=>$this->dog_id]);
			$this->appointment_id = self::$_connection->lastInsertId();
		}

		
		# As a shelter, I can reschedule an appointment.
		public function update(){
			$sql = "UPDATE Appointment 
			SET date_time=:date_time, address=:address, comment=:comment WHERE appointment_id=:appointment_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['date_time'=>$this->date_time,'address'=>$this->address,'comment'=>$this->comment, 'appointment_id'=>$this->appointment_id]);
		}

		# Deletes an appointment.
		public function delete(){
			$sql = "DELETE FROM Appointment WHERE appointment_id = :appointment_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['appointment_id'=>$this->appointment_id]);
		}

		# Returns an appointment according to the appointment_id passed as parameter.
		public function findAppointment($id){
			$sql = "SELECT * FROM Appointment WHERE appointment_id =:appointment_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['appointment_id'=>$id]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Appointment");
			return $sth->fetch(); 
		}

		# Returns all appointments from the Appointment table.
		public function getAll(){
			$sql = "SELECT * FROM Appointment ORDER BY date_time";
			$sth = self::$_connection->prepare($sql);
			$sth->execute();
			$sth->setFetchMode(PDO::FETCH_CLASS, "Appointment");
			return $sth->fetchAll(); 
		}

		# A person can view their list of appointments.
		public function getPersonAppointments(){
			$sql = "SELECT * FROM Appointment WHERE person_id = :person_id ORDER BY date_time";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['person_id'=>$_SESSION['profile_id']]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Appointment");
			return $sth->fetchAll(); 
		}

		# A shelter can see their list of appointments.
		public function getShelterAppointments(){
			$sql = "SELECT * FROM Appointment WHERE shelter_id = :shelter_id ORDER BY date_time";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['shelter_id'=>$_SESSION['profile_id']]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Appointment");
			return $sth->fetchAll(); 
		}
	}
?>