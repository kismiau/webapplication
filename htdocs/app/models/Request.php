<?php
	# Class to communicate information from the RequestController to the database itself.
	class Request extends Model {
		public $request_id;
		public $dog_id;
		public $person_id;
		public $status;
		
		# Default constructor
		public function __contruct() {
			parent::__construct(); 
		}

		
		# A person can send a request to adopt the dog.
		public function insert() { 
			$sql = "INSERT INTO Request (dog_id, person_id, status) VALUES(:dog_id, :person_id, :status)";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['dog_id'=>$this->dog_id, 'person_id'=>$this->person_id, 'status'=>$this->status]);
			$this->request_id = self::$_connection->lastInsertId();
		}

		
		# As a person, I can see a list of my requests.
		public function getPersonRequests() { 
			$sql = "SELECT * FROM Request WHERE person_id = :person_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['person_id'=>$_SESSION['profile_id']]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Request");
			return $sth->fetchAll(); 

		}

		# A shelter can see a list of incoming requests for adoption.
		public function getShelterRequests() { 
			$sql = "SELECT * FROM Request where dog_id in (SELECT dog_id from Dog WHERE shelter_id = :shelter_id)";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['shelter_id'=>$_SESSION['profile_id']]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Request");
			return $sth->fetchAll(); 
		}

		# As a person, I can cancel my request.
		public function delete() { 
			$sql = "DELETE FROM Request WHERE request_id = :request_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['request_id'=>$this->request_id]);
		}

		# A shelter can accept or decline a request for adoption.
		public function updateStatus($status) { 
			if ($status == 'Pending' || $status == 'Accepted' || $status == 'Denied' ){
				$sql = "UPDATE Request SET status = :status WHERE request_id = :request_id";
				$sth = self::$_connection->prepare($sql);
				$sth->execute(['status'=>$status,'request_id'=>$this->request_id]);
				return true;
			}
			return false;
		}

		# Returns a request for a certain request_id
		public function findRequest($id){
			$sql = "SELECT * FROM Request WHERE request_id =:request_id";
			$sth = self::$_connection->prepare($sql);
			$sth->execute(['request_id'=>$id]);
			$sth->setFetchMode(PDO::FETCH_CLASS, "Request");
			return $sth->fetch(); 
		}
	}
?>