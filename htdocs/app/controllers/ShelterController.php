<?php
	# Class to communicate Shelter model information between the model and the views
	class ShelterController extends Controller{
		
		# A user can register as a new shelter.
		public function Create(){
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Shelter'){
					if (isset($_POST['action'])){
						$current = $this->model('Shelter');
						
						if (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) 
		  					return $this->view('Shelter/Create', ['ivalidName'=>'Invalid name, only letters and whiste spaces allowed']);
						$current->name = $_POST['name'];
						$current->address = $_POST['address'];
						if (!preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/", $_POST['phone']))
							return $this->view('Shelter/Create', ['invalidPhoneNo'=>'Invalid phone number']);
						$current->phone = $_POST['phone'];
						if (filter_var($_POST['website'], FILTER_VALIDATE_URL) !== false){
							$current->website = $_POST['website'];
						}
						else{
							return $this->view('Shelter/Create', ['invalidWebsite'=>'Invalid web site address']);
						}

						$target_dir = "pictures/";
						$allowedTypes = array("jpg", "png", "gif", "jpeg");
						$max_upload = 5000000;
						if (isset($_FILES['picture'])){
							$the_file = $_FILES['picture'];

							$check = getimagesize($the_file["tmp_name"]);
							if ($check !== false){
								$extension = strtolower(pathinfo(basename($the_file["name"]), PATHINFO_EXTENSION));
								$target_file_name = uniqid().'.'.$extension;
								$target_path = $target_dir.$target_file_name;
								if ($the_file["size"] < $max_upload){
									move_uploaded_file($the_file["tmp_name"], $target_path);
									$current->picture = $target_file_name;
								}
								else
									return $this->view('Shelter/Create', ['invalidPic'=>'The image is too big']);
							}
							else
								return $this->view('Shelter/Create', ['invalidPic'=>'Something wrong with your image']);
						}
						else
							return $this->view('Shelter/Create', ['invalidPic'=>'You did not set an image']);
						try{
							$current->insert();
							$_SESSION['profile_id'] = $current->shelter_id;
							header('location:/Dog/Index');
						}
						catch(Exception $e){
							return $this->view('Shelter/Create', ['errormessage' => $e->getMessage()]);
						}
						
					}
					return $this->view('Shelter/Create');
				}
				else{
					return $this->view('Person/Create');
				}
			}
			return header('location:/User/Login');
		}

		# As a shelter, I can modify my profile.
		public function Edit(){
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Shelter'){
					if (isset($_POST['action'])){
						$current = $this->model('Shelter')->findShelter($_SESSION['profile_id']);
						if (!empty($_POST['name'])){
							if (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) 
		  						return $this->view('Shelter/Edit', ['ivalidName'=>'Invalid name, only letters and whiste spaces allowed']);
							$current->name = $_POST['name'];
						}
						if (!empty($_POST['address'])){
							$current->address = $_POST['address'];
						}
						if (!empty($_POST['phone'])){
							if (!preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/", $_POST['phone']))
								return $this->view('Shelter/Edit', ['invalidPhoneNo'=>'Invalid phone number']);
							$current->phone = $_POST['phone'];
						}
						if (!empty($_POST['website'])){
							if (filter_var($_POST['website'], FILTER_VALIDATE_URL) !== false){
								$current->website = $_POST['website'];
							}
						}

						$target_dir = "pictures/";
						$allowedTypes = array("jpg", "png", "gif", "jpeg");
						$max_upload = 5000000;
						if (!empty($_FILES['picture']['name'])){
							$the_file = $_FILES['picture'];
							$check = getimagesize($the_file["tmp_name"]);
							if ($check !== false){
								$extension = strtolower(pathinfo(basename($the_file["name"]), PATHINFO_EXTENSION));
								$target_file_name = uniqid().'.'.$extension;
								$target_path = $target_dir.$target_file_name;
								if ($the_file["size"] < $max_upload){
									move_uploaded_file($the_file["tmp_name"], $target_path);
									$current->picture = $target_file_name;
								}
								else
									return $this->view('Shelter/Edit', ['invalidPic'=>'The image is too big']);
							}
							else
								return $this->view('Shelter/Edit', ['invalidPic'=>'Something wrong with your image']);
						}

						$current->update();
						return header('location:/Shelter/Details/'.$_SESSION['profile_id']);
					}
					elseif (isset($_POST['cancel'])) {
						return $this->view('Shelter/Details', ['profile'=>$current]);
					}
					return $this->view('Shelter/Edit');
				}
				else
					return $this->view('Shelter/Details');
			}
			return header('location:/User/Login');
		}

		# As a shelter, I can see my profile.
		public function Details($id = 1){ 
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Shelter')
					$shelter_id = $_SESSION['profile_id'];
				else
					$shelter_id = $id;
				$shelter = $this->model('Shelter')->findShelter($shelter_id);
				if ($shelter != null){
					return $this->view('Shelter/Details', ['profile' => $shelter]);
				}else{
					return header('location:/Dog/Index');
				}
			}
			return header('location:/User/Login');
		}

		# As a shelter, I can delete my profile and all my dogs.
		public function Delete($response='W'){
			if (isset($_SESSION['user_id'])){
				$current = $this->model('Shelter')->findShelter($_SESSION['profile_id']);
				if ($response=='Y'){
					$worked = $current->delete();
					if ($worked)
						return header('location:/User/Login');
					else
						return $this->view('Shelter/Details', ['error_message' => 'You still have some appointments left.']);
				}
				elseif ($response=='N') {
					return header('location:/Shelter/Details/');
				}
				else
					return $this->view('Shelter/Details', ['profile' => $current, 'confirmation' => 'Are you sure?']);
			}
			return header('location:/User/Login');
		}
	}
?>
