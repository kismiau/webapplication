<?php
	# Class to communicate Request model information between the model and the views
	class RequestController extends Controller {

		# A person can send a request to adopt the dog.
		public function Create($dog_id=1) {
			if (isset($_SESSION['user_id'])){
				if($_SESSION['role'] == 'Person'){
					$current = $this->model('Request');
					$current->dog_id = $dog_id;
					$current->person_id = $_SESSION['profile_id'];
					$current->status = 'Pending';
					try{
						$current->insert();
						return header("location:/Request/Index");
					}
					catch(Exception $e){
						return $this->view('/Request/Index',['error'=>var_dump($current)]);
					}
				}
				else
					return header("location:/Request/Index");
			}
			return header('location:/User/Login');
		}

		# A shelter can accept or decline a request for adoption.
		public function EditStatus($request,$status) { 
			if (isset($_SESSION['user_id'])){
				if($_SESSION['role'] == 'Shelter'){
					$current = $this->model('Request')->findRequest($request);
					$current->updateStatus($status);
				}
				return header("location:/Request/Index");
			}
			return header('location:/User/Login');
		}

		# As a person, I can cancel my request.
		public function Delete($request) {
			if (isset($_SESSION['user_id'])){
				if($_SESSION['role'] == 'Person'){
					$current = $this->model('Request')->findRequest($request);
					$current->delete();
				}
				return header("location:/Request/Index");
			}
			return header('location:/User/Login');
		}

		# A shelter can see a list of incoming requests for adoption.
		# As a person, I can see a list of my requests.
		public function Index() { 
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Shelter'){
					$requests = $this->model('Request')->getShelterRequests();
				}
				elseif ($_SESSION['role'] == 'Person') {
					$requests = $this->model('Request')->getPersonRequests();
				}
				else{
					return header('location:/User/Login');
				}
				return $this->view('/Request/Index', ['requests'=>$requests]);
			}
			return header('location:/User/Login');
		}
	}
?>