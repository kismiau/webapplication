<?php
	# Class to communicate Dog model information between the model and the views
	class DogController extends Controller {
		
		# A person can see details of a selected dog.
		# As a shelter, I can see details of a selected dog.
		public function Details($dog_id) {
			if (isset($_SESSION['user_id'])){
				$dog = $this->model('Dog');
				$dog->dog_id = $dog_id;
				$dog = $dog->find($dog_id);
				if($dog != null) {
					return $this->view('Dog/Details', ['dog' => $dog]);
				}
				else{
					return header('location:/Dog/Index');
				}
			}else 
				return header('location:/User/Login');
		}
		
		
		# A shelter can add a dog to their collection.
		public function Create() {
			if (isset($_SESSION['user_id'])){
				if(isset($_POST['action'])) {
					$dog = $this->model('Dog');

					if (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) 
						return $this->view('Dog/Create', ['ivalidfName'=>'Invalid name, only letters and white spaces allowed']);
					$dog->name = $_POST['name'];
					if (!preg_match("/^[0-9]*$/",$_POST['age'])) 
						return $this->view('Dog/Create', ['invalidAge'=>'Invalid age, only numbers allowed']);
					$dog->age = $_POST['age'];
					$dog->gender = $_POST['gender'];
					if (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) 
						return $this->view('Dog/Create', ['ivalidfName'=>'Invalid color, only letters and white spaces allowed']);
					$dog->color = $_POST['color'];

					//---Picture Upload
					$target_dir = "pictures/";
					$allowedTypes = array("jpg", "png", "gif", "jpeg");
					$max_upload = 5000000;
					if (isset($_FILES['picture'])){
						$the_file = $_FILES['picture'];
						if(isset($the_file["tmp_name"])) {
							$check = getimagesize($the_file["tmp_name"]);
							if ($check !== false){
								$extension = strtolower(pathinfo(basename($the_file["name"]), PATHINFO_EXTENSION));
								$target_file_name = uniqid().'.'.$extension;
								$target_path = $target_dir.$target_file_name;
								if ($the_file["size"] < $max_upload){
									move_uploaded_file($the_file["tmp_name"], $target_path);
									$dog->picture = $target_file_name;
								}
								else
									return $this->view('Dog/Create', ['invalidPic'=>'The image is too big']);
							}
							else
								return $this->view('Dog/Create', ['invalidPic'=>'Something wrong with your image']);
						}
					}
					else {
						return $this->view('Dog/Create', ['invalidPic'=>'You did not set an image']);
					}


					if($_POST['isTrained']=='Yes')
						$dog->isTrained = true;
					else 
						$dog->isTrained = false;
					if($_POST['experienceLevel'] < 1 || $_POST['experienceLevel'] > 10)
						return $this->view('Dog/Create', ['invalidLevel'=>'Invalid level, please enter a number from 1 to 10!']); 
					$dog->experienceLevel = $_POST['experienceLevel'];
					$dog->description = $_POST['description'];
						
					$size = $_POST['size'];
					$dog->size_id = $this->model('Size')->findSizeId($size);

					$dog->shelter_id = $_SESSION['profile_id'];
					
					try{
						$dog->dog_id = $dog->insert();
						$this->insertMixBreed($dog->dog_id);
						header('location:/Dog/Index');
					}
					catch(Exception $e){
						$this->view('Dog/Create', ['errormessage' => 'There was an error.']);
						return $this->view('Dog/Create', ['errormessage' => $e->getMessage()]);
					}
				}
				else {
					$breeds = $this->model('Breed')->getAll();
					$this->view('Dog/Create', ['breeds' => $breeds]); 
				}
			}
			else
				return header('location:/User/Login');
		}
		
		
		# A shelter can edit a dog’s characteristics.
		public function Edit($dog_id) { 
			if (isset($_SESSION['user_id'])){
				$dog = $this->model('Dog');
				$dog->dog_id = $dog_id;
				$dog = $dog->find($dog_id);

				if(isset($_POST['action'])) {
					if (!empty($_POST['name'])){
						if (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) 
							return $this->view('Dog/Edit', ['dog'=>$dog, 'ivalidfName'=>'Invalid name, only letters and white spaces allowed']);
						$dog->name = $_POST['name'];
					}
					
					if (!empty($_POST['age'])){
						if (!preg_match("/^(\d*\.)?\d+$/", $_POST['age']))  
							return $this->view('Dog/Edit', ['dog'=>$dog, 'invalidAge'=>'Invalid age, only numbers allowed']);
						$dog->age = $_POST['age'];
					}
					$dog->gender = $_POST['gender'];

					if (!empty($_POST['color'])){
						if (!preg_match("/^[a-zA-Z ]*$/",$_POST['name'])) 
							return $this->view('Dog/Edit', ['dog'=>$dog, 'ivalidfName'=>'Invalid color, only letters and white spaces allowed']);
						$dog->color = $_POST['color'];
					}
					
					$target_dir = "pictures/";
					$allowedTypes = array("jpg", "png", "gif", "jpeg");
					$max_upload = 5000000;
					if (!empty($_FILES['picture']['name'])){
						$the_file = $_FILES['picture'];
						$check = getimagesize($the_file["tmp_name"]);
						if ($check !== false){
							$extension = strtolower(pathinfo(basename($the_file["name"]), PATHINFO_EXTENSION));
							$target_file_name = uniqid().'.'.$extension;
							$target_path = $target_dir.$target_file_name;
							if ($the_file["size"] < $max_upload){
								move_uploaded_file($the_file["tmp_name"], $target_path);
								$dog->picture = $target_file_name;
							}
							else
								return $this->view('Person/Edit', ['dog'=>$dog, 'invalidPic'=>'The image is too big']);
						}
						else
							return $this->view('Person/Edit', ['dog'=>$dog, 'invalidPic'=>'Something wrong with your image']);
					}
		

					if($_POST['isTrained']=='Yes')
						$dog->isTrained = true;
					else 
						$dog->isTrained = false;

					if (!empty($_POST['experienceLevel'])){
						if($_POST['experienceLevel'] < 1 || $_POST['experienceLevel'] > 10)
							return $this->view('Dog/Edit', ['invalidLevel'=>'Invalid level, please enter a number from 1 to 10!']); 
						$dog->experienceLevel = $_POST['experienceLevel'];
					}
					if (!empty($_POST['experienceLevel'])){
						$dog->description = $_POST['description'];
					}

					$size = $_POST['size'];
					$dog->size_id = $this->model('Size')->findSizeId($size);				

					$dog->shelter_id = $_SESSION['profile_id'];
					try {
						$dog->edit();
						$this->insertMixBreed($dog->dog_id);
						$this->view('Dog/Details', ['dog' => $dog]);
					}
					catch(Exception $e) {
						$this->view('Dog/Edit', ['errormessage' => 'There was an error.']);
						return $this->view('Dog/Edit', ['errormessage' => $e->getMessage()]);
					}
				}
				else {
					return $this->view('Dog/Edit', ['dog' => $dog]);
				}
			}
			else
				return header('location:/User/Login');
		}
		
		
		# A shelter can remove a dog from their collection.
		public function Delete($dog_id,$response='W') {
			if (isset($_SESSION['user_id'])){
				$dog = $this->model('Dog')->find($dog_id);
				if ($response=='Y'){
					$worked = $dog->deleteOne();
					if ($worked)
						return header('location:/Dog/Index');
					else
						return $this->view('Dog/Details', ['dog' => $dog, 'error_message' => 'Unable to delete dog.']);
				}
				elseif ($response=='N') {
					return $this->view('Dog/Details', ['dog' => $dog, 'confirmation' => '']);
				}
				else
					return $this->view('Dog/Details', ['dog' => $dog, 'confirmation' => 'Are you sure?']);
			}
			else
				return header('location:/User/Login');
		}
		
		# A shelter can see a list of their dogs.
		# A user can see a list of all dogs.
		public function Index() {
			if (isset($_SESSION['user_id'])){
				$dog = $this->model('Dog');
				$dogs = $dog->getAllDogs();
				$this->view('Dog/Index', ['dogs' => $dogs]); 
			}
			else
				return header('location:/User/Login');
		}

		# Inserts a list of breeds for one dog in the database. Ensures to delete breeds that are already there beforehand, like updating the information.
		public function insertMixBreed($dog_id) { 
			if (isset($_SESSION['user_id'])){
				$mixbreed = $this->model('MixBreed');
				if(isset($_POST['mixbreeds'])) {
					$this->model('MixBreed')->deleteOne($dog_id);
					$breeds = $_POST['mixbreeds'];
					foreach($breeds as $breed) {
						$mixbreed->breed_id = $breed;
						$mixbreed->dog_id = $dog_id;
						$mixbreed->insert();
					}
				}
			}
			else
				return header('location:/User/Login');
		}
		
		
		# A person can search for dogs by different characteristics and display these dogs.
		public function Search() {
			if (isset($_SESSION['user_id'])){
				$dog = $this->model('Dog');
				$dogs = $dog->getAllDogs();

				if(isset($_POST['action'])) {
					if(isset($_POST['category']) && isset($_POST['search'])) {
						$category = $_POST['category'];
						$input = $_POST['search'];
						if($category=='name') {
							$dogs = $dog->searchName($input);
						}
						elseif($category=='age') {
							$dogs = $dog->searchAge($input);
						}
						elseif($category=='gender') {
							$dogs = $dog->searchGender($input);
						}
						elseif($category=='color') {
							$dogs = $dog->searchColor($input);
						}
						elseif($category=='size') {
							$dogs = $dog->searchSize($input);
						}
						elseif($category=='breed') {
							$dogs = $dog->searchBreed($input);
						}
						elseif($category=='isTrained') {
							$dogs = $dog->searchIsTrained($input);
						}
						elseif($category=='experienceLevel') {
							$dogs = $dog->searchExperienceLevel($input);
						}
						elseif($category=='description') {
							$dogs = $dog->searchDescription($input);
						}
					}
					$this->view('Dog/Index', ['dogs' => $dogs]);
				}
				else {
					$this->view('Dog/Index', ['dogs' => $dogs]);
				}
			}
			else
				return header('location:/User/Login');
		}

		
		# A shelter can order his dogs by characteristics.
		public function Order() {
			if (isset($_SESSION['user_id'])){
				$dog = $this->model('Dog');
				if(isset($_POST['action']) ) {
					if(isset($_POST['order'])) {
						$category = $_POST['order'];
						$dogs = $dog->getAllOrderedDogs($category);
						return $this->view('Dog/Index', ['dogs' => $dogs]);
					}
				}
				else {
					return header('location:/Dog/Index');
				}
			}
			else
				return header('location:/User/Login');
		}
	}
?>