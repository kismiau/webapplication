<?php
	# Class to communicate Person model information between the model and the views
	class PersonController extends Controller{
		
		# A user can register as a new person.
		public function Create(){
			if (isset($_SESSION['user_id'])){
				if($_SESSION['role'] == 'Person'){
					if (isset($_POST['action'])){
						$current = $this->model('Person');
						
						if (!preg_match("/^[a-zA-Z ]*$/",$_POST['f_name'])) 
		  					return $this->view('Person/Create', ['ivalidfName'=>'Invalid first name, only letters and whiste spaces allowed']);
						$current->f_name = $_POST['f_name'];

						if (!preg_match("/^[a-zA-Z ]*$/",$_POST['l_name'])) 
		  					return $this->view('Person/Create', ['ivalidlName'=>'Invalid last name, only letters and whiste spaces allowed']);
						$current->l_name = $_POST['l_name'];
						if (!preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/", $_POST['phone']))
							return $this->view('Person/Create', ['invalidPhoneNo'=>'Invalid phone number']);
						$current->phone = $_POST['phone'];
						
						$target_dir = "pictures/";
						$allowedTypes = array("jpg", "png", "gif", "jpeg");
						$max_upload = 5000000;
						if (isset($_FILES['picture'])){
							$the_file = $_FILES['picture'];

							$check = getimagesize($the_file["tmp_name"]);
							if ($check !== false){
								$extension = strtolower(pathinfo(basename($the_file["name"]), PATHINFO_EXTENSION));
								$target_file_name = uniqid().'.'.$extension;
								$target_path = $target_dir.$target_file_name;
								if ($the_file["size"] < $max_upload){
									move_uploaded_file($the_file["tmp_name"], $target_path);
									$current->picture = $target_file_name;
								}
								else
									return $this->view('Person/Create', ['invalidPic'=>'The image is too big']);
							}
							else
								return $this->view('Person/Create', ['invalidPic'=>'Something wrong with your image']);
						}
						else
							return $this->view('Person/Create', ['invalidPic'=>'You did not set an image']);
						try{
							$current->insert();
							$_SESSION['profile_id'] = $current->person_id;
							header('location:/Dog/Index');
						}
						catch(Exception $e){
							return $this->view('Person/Create', ['errormessage' => $e->getMessage()]);
						}
						
					}
					return $this->view('Person/Create');
				}
				else{
					return $this->view('Shelter/Create');
				}
			}
			return header('location:/User/Login');
		}

		# As a person, I can modify my profile.
		public function Edit(){
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Person'){
					if (isset($_POST['action'])){
						$current = $this->model('Person')->findPerson($_SESSION['profile_id']);
						if (!empty($_POST['f_name'])){
							if (!preg_match("/^[a-zA-Z ]*$/",$_POST['f_name'])) 
		  						return $this->view('Person/Edit', ['ivalidfName'=>'Invalid first name, only letters and whiste spaces allowed']);
							$current->f_name = $_POST['f_name'];
						}
						if (!empty($_POST['l_name'])){
							if (!preg_match("/^[a-zA-Z ]*$/",$_POST['l_name'])) 
			  					return $this->view('Person/Edit', ['ivalidlName'=>'Invalid last name, only letters and whiste spaces allowed']);
							$current->l_name = $_POST['l_name'];
						}
						if (!empty($_POST['phone'])){
							if (!preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/", $_POST['phone']))
								return $this->view('Person/Edit', ['invalidPhoneNo'=>'Invalid phone number']);
							$current->phone = $_POST['phone'];
						}

						$target_dir = "pictures/";
						$allowedTypes = array("jpg", "png", "gif", "jpeg");
						$max_upload = 5000000;
						if (!empty($_FILES['picture']['name'])){
							$the_file = $_FILES['picture'];
							$check = getimagesize($the_file["tmp_name"]);
							if ($check !== false){
								$extension = strtolower(pathinfo(basename($the_file["name"]), PATHINFO_EXTENSION));
								$target_file_name = uniqid().'.'.$extension;
								$target_path = $target_dir.$target_file_name;
								if ($the_file["size"] < $max_upload){
									move_uploaded_file($the_file["tmp_name"], $target_path);
									$current->picture = $target_file_name;
								}
								else
									return $this->view('Person/Edit', ['invalidPic'=>'The image is too big']);
							}
							else
								return $this->view('Person/Edit', ['invalidPic'=>'Something wrong with your image']);
						}

						$current->update();
						return header('location:/Person/Details/'.$_SESSION['profile_id']);
					}
					elseif (isset($_POST['cancel'])) {
						return $this->view('Person/Details', ['profile'=>$current]);
					}
					return $this->view('Person/Edit');
				}
				else
					return $this->view('Person/Details');
			}
			return header('location:/User/Login');
		}

		# A person can see their profile.
		public function Details($id = 1){ 
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Person')
					$person_id = $_SESSION['profile_id'];
				else
					$person_id = $id;
				$person = $this->model('Person')->findPerson($person_id);
				if ($person != null){
					return $this->view('Person/Details', ['profile' => $person]);
				}else{
					return header('location:/Dog/Index');
				}
			}
			return header('location:/User/Login');
		}

		# As a person, I can delete my profile.
		public function Delete($response='W'){
			if (isset($_SESSION['user_id'])){
				$current = $this->model('Person')->findPerson($_SESSION['profile_id']);
				if ($response=='Y'){
					$worked = $current->delete();
					if ($worked)
						return header('location:/User/Login');
					else
						return $this->view('Person/Details', ['profile' => $current,'error_message' => 'You still have some appointments left.']);
				}
				elseif ($response=='N') {
					return header('location:/Person/Details/');
					//return $this->view('Person/Details', ['profile' => $current, 'confirmation' => '']);
				}
				else
					return $this->view('Person/Details', ['profile' => $current, 'confirmation' => 'Are you sure?']);
			}
			return header('location:/User/Login');
		}
	}

?>


