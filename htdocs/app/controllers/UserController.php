<?php
# Class to communicate User model information between the model and the views
class UserController extends Controller{

	public function Index(){
		return header('location:/User/Login');
	}

	public function Create(){
		$this->view('User/Create');
	}
	
	
	# A user can login as a person.
	# A user can login as a shelter.
	public function login(){
		if (!isset($_SESSION['user_id']))
		{
			if(isset($_POST['action'])){
				$password = $_POST['password'];
				if (isset($_POST['username'])){
					$user = $this->model('User')->findUser("".$_POST['username']);
					if($user != null){
						if(password_verify($password, $user->password)){ 
							$_SESSION['role'] = $user->role;
							$_SESSION['username'] = $user->username;
							$_SESSION['user_id'] = $user->user_id;
							$profile = $this->model($user->role)->findProfile($user->user_id);
							if (!empty($profile)){
								if ($user->role == 'Shelter'){
									$_SESSION['profile_id'] = $profile->shelter_id;
								}
								else{
									$_SESSION['profile_id'] = $profile->person_id;
								}
								
								header('location:/Dog/Index');
							}
								
							else
								header("location:/$user->role/Create");
						}
						else{
							return $this->view('User/Login',['invalidPassword'=>'Invalid password.']);
						}
					}
					else
						return $this->view('User/Login',['error'=>'Invalid username or password.']);
				}
			}
			else{
				return $this->view('User/Login');
			}
		}
		else
		{
			header('location:/Dog/Index');
		}
		
	}

	
	# A user can register as a new shelter
	# A user can register as a new person
	public function signup(){
		if(isset($_POST['action'])){
			if ($_POST['password'] != $_POST['confirm']){
				return $this->view('User/Create', ['notMatch' => 'Passwords did not match ']);
			}
			elseif (!filter_var($_POST['username'], FILTER_VALIDATE_EMAIL)) {
				return $this->view('User/Create', ['invalidEmail' =>  'Please enter a valid email address']);
			}
			else{
				$user = $this->model('User');
				$user->username = $_POST['username'];
				$user->password = password_hash($_POST['password'],PASSWORD_DEFAULT);
				$user->role = $_POST['role'];
				try{
					$user->insert();
					header('location:/User/Login');
				}
				catch(Exception $e){
					return $this->view('User/Create', ['errormessage' => 'The username you have entered is already in the database.']);
				}
			}
		}
		else
			return $this->view('User/Create', ['another' => 'Something went wrong']);
	}

	# A user can logout.
	public function logout(){
		$_SESSION = array();
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		session_destroy();
		header('location:/User/login');
	}
}

?>