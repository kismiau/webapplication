<?php
	# Class to communicate Appointment model information between the model and the views
	class AppointmentController extends Controller {
		# A shelter can see their list of appointments.
		# A person can view their list of appointments.
		public function Index() { 
			if (isset($_SESSION['user_id'])){
				$this->DeleteObsolete();
				if ($_SESSION['role'] == 'Person'){
					$appointments = $this->model('Appointment')->getPersonAppointments();
				}
				elseif ($_SESSION['role'] == 'Shelter') {
					$appointments = $this->model('Appointment')->getShelterAppointments();
				}
				else{
					return header('location:/User/Login');
				}
				return $this->view('/Appointment/Index', ['appointments'=>$appointments]);
			} 
				return header('location:/User/Login');
		}

		# As a person, I can set an appointment.
		public function Create($dog_id) {
			if (isset($_SESSION['user_id'])){
				if (isset($_POST['action'])){
					$dog = $this->model('Dog')->find($dog_id);
					$shelter = $this->model('Shelter')->findShelter($dog->shelter_id);
					$person = $this->model('Person')->findPerson($_SESSION['profile_id']);

					$current = $this->model('Appointment');

					$current->address = $_POST['address'];
					$current->date_time = new DateTime($_POST['datetime']);
					$current->date_time = $current->date_time->format('Y-m-d H:i:s');
					$current->comment = $_POST['comment'];
					$current->shelter_id = $shelter->shelter_id;
					$current->person_id = $person->person_id;
					$current->dog_id = $dog_id;
					try{
						$current->insert();
						return header("location:/Appointment/Index");
					}
					catch(Exception $e){
						return $this->view('/Appointment/Create',['error'=>var_dump($current)]);
					}
				}
				return $this->view('/Appointment/Create',['dog_id'=>$dog_id]);
			}
			return header('location:/User/Login');
		}

		# As a shelter, I can reschedule an appointment.
		public function Edit($id=0) {
			if (isset($_SESSION['user_id'])){
				if ($_SESSION['role'] == 'Shelter'){
					$current = $this->model('Appointment')->findAppointment($id);
					if (isset($_POST['action'])){
						if ($_POST['address'] != ''){
							$current->address = $_POST['address'];
						}
						if ($_POST['datetime'] != '')
						{
							$current->date_time = new DateTime($_POST['datetime']);
							$current->date_time = $current->date_time->format('Y-m-d H:i:s');
						}
						if ($_POST['comment'] != ''){
							$current->comment = $_POST['comment'];
						}
						try{
							$current->update();
							header("location:/Appointment/Index");
						}
						catch(Exception $e){
							return $this->view('/Appointment/Edit',['error'=>var_dump($current)]);
						}
					}

					return $this->view('/Appointment/Edit', ['appointment'=>$current]);
				}
				else
					return header("location:/Appointment/Index");
			}
			return header('location:/User/Login');
		}

		# Deletes all appointments that are past today's date.
		public function DeleteObsolete() {
			$todaydate = strtotime("Today");
			$appointments = $this->model('Appointment')->getAll();
			foreach($appointments as $appointment) {
				$appodate = strtotime($appointment->date_time);
				if($todaydate > $appodate)
					$appointment->delete();
			}
		}
	}
?>