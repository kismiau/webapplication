-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2018 at 07:09 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pawscouts`
--
CREATE DATABASE IF NOT EXISTS `pawscouts` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pawscouts`;

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `address` varchar(50) NOT NULL,
  `comment` varchar(50) NOT NULL,
  `shelter_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `dog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`appointment_id`, `date_time`, `address`, `comment`, `shelter_id`, `person_id`, `dog_id`) VALUES
(4, '2018-12-11 06:30:00', '3550 St Victoria', 'Aloha', 2, 3, 1),
(5, '2018-12-26 17:00:00', '1893 Jean Brillant', 'Hello, Im ready to pick him up, please leave  me a', 3, 4, 2),
(6, '2018-12-22 18:00:00', '2121 Cote Saint Luc', 'Im ready!', 5, 5, 6),
(7, '2018-12-07 18:00:00', '5050 St Leonarde', 'Hopes to see you soon!', 4, 6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

DROP TABLE IF EXISTS `breed`;
CREATE TABLE `breed` (
  `breed_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`breed_id`, `name`) VALUES
(1, 'ENGLISH POINTER'),
(2, 'ENGLISH SETTER'),
(3, 'KERRY BLUE TERRIER'),
(4, 'CAIRN TERRIER'),
(5, 'ENGLISH COCKER SPANIEL'),
(6, 'GORDON SETTER'),
(7, 'AIREDALE TERRIER'),
(8, 'AUSTRALIAN TERRIER'),
(9, 'BEDLINGTON TERRIER'),
(10, 'BORDER TERRIER'),
(11, 'BULL TERRIER'),
(12, 'FOX TERRIER '),
(13, 'ENGLISH TOY TERRIER'),
(14, 'SWEDISH VALLHUND'),
(15, 'BELGIAN SHEPHERD DOG'),
(16, 'OLD ENGLISH SHEEPDOG'),
(17, 'GRIFFON NIVERNAIS'),
(19, 'BRIQUET GRIFFON VENDEEN'),
(20, 'ARIEGEOIS'),
(21, 'GASCON SAINTONGEOIS'),
(22, 'GREAT GASCONY BLUE'),
(24, 'POITEVIN'),
(25, 'BILLY'),
(28, 'ARTOIS HOUND'),
(30, 'PORCELAINE'),
(31, 'SMALL BLUE GASCONY BLUE'),
(32, 'BLUE GASCONY GRIFFON'),
(33, 'GRAND BASSET GRIFFON VENDEEN'),
(34, 'NORMAN ARTESIEN BASSET'),
(35, 'BLUE GASCONY BASSET'),
(36, 'BASSET FAUVE DE BRETAGNE'),
(37, 'PORTUGUESE WATER DOG'),
(38, 'WELSH CORGI CARDIGAN'),
(39, 'WELSH CORGI PEMBROKE'),
(40, 'IRISH SOFT COATED WHEATEN TERRIER'),
(41, 'YUGOSLAVIAN SHEPHERD DOG - SHARPLANINA'),
(42, 'JÄMTHUND'),
(43, 'BASENJI'),
(44, 'BERGER DE BEAUCE'),
(45, 'BERNESE MOUNTAIN DOG'),
(46, 'APPENZELL CATTLE DOG'),
(47, 'ENTLEBUCH CATTLE DOG'),
(48, 'KARELIAN BEAR DOG'),
(49, 'FINNISH SPITZ'),
(50, 'NEWFOUNDLAND'),
(51, 'FINNISH HOUND'),
(52, 'POLISH HOUND'),
(53, 'KOMONDOR'),
(54, 'KUVASZ'),
(55, 'PULI'),
(56, 'PUMI'),
(57, 'HUNGARIAN SHORT-HAIRED POINTER'),
(58, 'GREAT SWISS MOUNTAIN DOG'),
(59, 'SWISS HOUND'),
(60, 'SMALL SWISS HOUND'),
(61, 'ST. BERNARD'),
(62, 'COARSE-HAIRED STYRIAN HOUND'),
(63, 'AUSTRIAN BLACK AND TAN HOUND'),
(64, 'AUSTRIAN  PINSCHER'),
(65, 'MALTESE'),
(66, 'FAWN BRITTANY GRIFFON'),
(67, 'PETIT BASSET GRIFFON VENDEEN'),
(68, 'TYROLEAN HOUND'),
(70, 'LAKELAND TERRIER'),
(71, 'MANCHESTER TERRIER'),
(72, 'NORWICH TERRIER'),
(73, 'SCOTTISH TERRIER'),
(74, 'SEALYHAM TERRIER'),
(75, 'SKYE TERRIER'),
(76, 'STAFFORDSHIRE BULL TERRIER'),
(77, 'CONTINENTAL TOY SPANIEL'),
(78, 'WELSH TERRIER'),
(80, 'GRIFFON BRUXELLOIS'),
(81, 'GRIFFON BELGE'),
(82, 'PETIT BRABANÇON'),
(83, 'SCHIPPERKE'),
(84, 'BLOODHOUND'),
(85, 'WEST HIGHLAND WHITE TERRIER'),
(86, 'YORKSHIRE TERRIER'),
(87, 'CATALAN SHEEPDOG'),
(88, 'SHETLAND SHEEPDOG'),
(89, 'IBIZAN PODENCO'),
(90, 'BURGOS POINTING DOG'),
(91, 'SPANISH MASTIFF'),
(92, 'PYRENEAN MASTIFF'),
(93, 'PORTUGUESE SHEEPDOG'),
(94, 'PORTUGUESE WARREN HOUND-PORTUGUESE PODENGO'),
(95, 'BRITTANY SPANIEL'),
(96, 'RAFEIRO OF ALENTEJO'),
(97, 'GERMAN SPITZ'),
(98, 'GERMAN WIRE- HAIRED POINTING DOG'),
(99, 'WEIMARANER'),
(100, 'WESTPHALIAN DACHSBRACKE'),
(101, 'FRENCH BULLDOG'),
(102, 'KLEINER MÜNSTERLÄNDER'),
(103, 'GERMAN HUNTING TERRIER'),
(104, 'GERMAN SPANIEL'),
(105, 'FRENCH WATER DOG'),
(106, 'BLUE PICARDY SPANIEL'),
(107, 'WIRE-HAIRED POINTING GRIFFON KORTHALS'),
(108, 'PICARDY SPANIEL'),
(109, 'CLUMBER SPANIEL'),
(110, 'CURLY COATED RETRIEVER'),
(111, 'GOLDEN RETRIEVER'),
(113, 'BRIARD'),
(114, 'PONT-AUDEMER SPANIEL'),
(115, 'SAINT GERMAIN POINTER'),
(116, 'DOGUE DE BORDEAUX'),
(117, 'DEUTSCH LANGHAAR'),
(118, 'LARGE MUNSTERLANDER'),
(119, 'GERMAN SHORT- HAIRED POINTING DOG'),
(120, 'IRISH RED SETTER'),
(121, 'FLAT COATED RETRIEVER'),
(122, 'LABRADOR RETRIEVER'),
(123, 'FIELD SPANIEL'),
(124, 'IRISH WATER SPANIEL'),
(125, 'ENGLISH SPRINGER SPANIEL'),
(126, 'WELSH SPRINGER SPANIEL'),
(127, 'SUSSEX SPANIEL'),
(128, 'KING CHARLES SPANIEL'),
(129, 'SMÅLANDSSTÖVARE'),
(130, 'DREVER'),
(131, 'SCHILLERSTÖVARE'),
(132, 'HAMILTONSTÖVARE'),
(133, 'FRENCH POINTING DOG - GASCOGNE TYPE'),
(134, 'FRENCH POINTING DOG - PYRENEAN TYPE'),
(135, 'SWEDISH LAPPHUND'),
(136, 'CAVALIER KING CHARLES SPANIEL'),
(137, 'PYRENEAN MOUNTAIN DOG'),
(138, 'PYRENEAN SHEEPDOG - SMOOTH FACED'),
(139, 'IRISH TERRIER'),
(140, 'BOSTON TERRIER'),
(141, 'LONG-HAIRED PYRENEAN SHEEPDOG'),
(142, 'SLOVAKIAN CHUVACH'),
(143, 'DOBERMANN'),
(144, 'BOXER'),
(145, 'LEONBERGER'),
(146, 'RHODESIAN RIDGEBACK'),
(147, 'ROTTWEILER'),
(148, 'DACHSHUND'),
(149, 'BULLDOG'),
(150, 'SERBIAN HOUND'),
(151, 'ISTRIAN SHORT-HAIRED HOUND'),
(152, 'ISTRIAN WIRE-HAIRED HOUND'),
(153, 'DALMATIAN'),
(154, 'POSAVATZ HOUND'),
(155, 'BOSNIAN BROKEN-HAIRED HOUND - CALLED BARAK'),
(156, 'COLLIE ROUGH'),
(157, 'BULLMASTIFF'),
(158, 'GREYHOUND'),
(159, 'ENGLISH FOXHOUND'),
(160, 'IRISH WOLFHOUND'),
(161, 'BEAGLE'),
(162, 'WHIPPET'),
(163, 'BASSET HOUND'),
(164, 'DEERHOUND'),
(165, 'ITALIAN SPINONE'),
(166, 'GERMAN SHEPHERD DOG'),
(167, 'AMERICAN COCKER SPANIEL'),
(168, 'DANDIE DINMONT TERRIER'),
(169, 'FOX TERRIER'),
(170, 'CASTRO LABOREIRO DOG'),
(171, 'BOUVIER DES ARDENNES'),
(172, 'POODLE'),
(173, 'ESTRELA MOUNTAIN DOG'),
(175, 'FRENCH SPANIEL'),
(176, 'PICARDY SHEEPDOG'),
(177, 'ARIEGE POINTING DOG'),
(179, 'BOURBONNAIS POINTING DOG'),
(180, 'AUVERGNE POINTER'),
(181, 'GIANT SCHNAUZER'),
(182, 'SCHNAUZER'),
(183, 'MINIATURE SCHNAUZER'),
(184, 'GERMAN PINSCHER'),
(185, 'MINIATURE PINSCHER'),
(186, 'AFFENPINSCHER'),
(187, 'PORTUGUESE POINTING DOG'),
(188, 'SLOUGHI'),
(189, 'FINNISH LAPPHUND'),
(190, 'HOVAWART'),
(191, 'BOUVIER DES FLANDRES'),
(192, 'KROMFOHRLÄNDER'),
(193, 'BORZOI - RUSSIAN HUNTING SIGHTHOUND'),
(194, 'BERGAMASCO SHEPHERD DOG'),
(195, 'ITALIAN VOLPINO'),
(196, 'BOLOGNESE'),
(197, 'NEAPOLITAN MASTIFF'),
(198, 'ITALIAN ROUGH-HAIRED SEGUGIO'),
(199, 'CIRNECO DELL\'ETNA'),
(200, 'ITALIAN GREYHOUND'),
(201, 'MAREMMA AND THE ABRUZZES SHEEPDOG'),
(202, 'ITALIAN POINTING DOG'),
(203, 'NORWEGIAN HOUND'),
(204, 'SPANISH HOUND'),
(205, 'CHOW CHOW'),
(206, 'JAPANESE CHIN'),
(207, 'PEKINGESE'),
(208, 'SHIH TZU'),
(209, 'TIBETAN TERRIER'),
(212, 'SAMOYED'),
(213, 'HANOVERIAN SCENTHOUND'),
(214, 'HELLENIC HOUND'),
(215, 'BICHON FRISE'),
(216, 'PUDELPOINTER'),
(217, 'BAVARIAN MOUNTAIN SCENT HOUND'),
(218, 'CHIHUAHUA'),
(219, 'FRENCH TRICOLOUR HOUND'),
(220, 'FRENCH WHITE & BLACK HOUND'),
(221, 'FRISIAN WATER DOG'),
(222, 'STABIJHOUN'),
(223, 'DUTCH SHEPHERD DOG'),
(224, 'DRENTSCHE PARTRIDGE DOG'),
(225, 'FILA BRASILEIRO'),
(226, 'LANDSEER'),
(227, 'LHASA APSO'),
(228, 'AFGHAN HOUND'),
(229, 'SERBIAN TRICOLOUR HOUND'),
(230, 'TIBETAN MASTIFF'),
(231, 'TIBETAN SPANIEL'),
(232, 'DEUTSCH STICHELHAAR'),
(233, 'LITTLE LION DOG'),
(234, 'XOLOITZCUINTLE'),
(235, 'GREAT DANE'),
(236, 'AUSTRALIAN SILKY TERRIER'),
(237, 'NORWEGIAN BUHUND'),
(238, 'MUDI'),
(239, 'HUNGARIAN WIRE-HAIRED POINTER'),
(240, 'HUNGARIAN GREYHOUND'),
(241, 'HUNGARIAN HOUND - TRANSYLVANIAN SCENT HOUND'),
(242, 'NORWEGIAN ELKHOUND GREY'),
(243, 'ALASKAN MALAMUTE'),
(244, 'SLOVAKIAN HOUND'),
(245, 'BOHEMIAN WIRE-HAIRED POINTING GRIFFON'),
(246, 'CESKY TERRIER'),
(247, 'ATLAS MOUNTAIN DOG'),
(248, 'PHARAOH HOUND'),
(249, 'MAJORCA MASTIFF'),
(250, 'HAVANESE'),
(251, 'POLISH LOWLAND SHEEPDOG'),
(252, 'TATRA SHEPHERD DOG'),
(253, 'PUG'),
(254, 'ALPINE DACHSBRACKE'),
(255, 'AKITA'),
(257, 'SHIBA'),
(259, 'JAPANESE TERRIER'),
(260, 'TOSA'),
(261, 'HOKKAIDO'),
(262, 'JAPANESE SPITZ'),
(263, 'CHESAPEAKE BAY RETRIEVER'),
(264, 'MASTIFF'),
(265, 'NORWEGIAN LUNDEHUND'),
(266, 'HYGEN HOUND'),
(267, 'HALDEN HOUND'),
(268, 'NORWEGIAN ELKHOUND BLACK'),
(269, 'SALUKI'),
(270, 'SIBERIAN HUSKY'),
(271, 'BEARDED COLLIE'),
(272, 'NORFOLK TERRIER'),
(273, 'CANAAN DOG'),
(274, 'GREENLAND DOG'),
(276, 'NORRBOTTENSPITZ'),
(277, 'CROATIAN SHEPHERD DOG'),
(278, 'KARST SHEPHERD DOG'),
(279, 'MONTENEGRIN MOUNTAIN HOUND'),
(281, 'OLD DANISH POINTING DOG'),
(282, 'GRAND GRIFFON VENDEEN'),
(283, 'COTON DE TULEAR'),
(284, 'LAPPONIAN HERDER'),
(285, 'SPANISH GREYHOUND'),
(286, 'AMERICAN STAFFORDSHIRE TERRIER'),
(287, 'AUSTRALIAN CATTLE DOG'),
(288, 'CHINESE CRESTED DOG'),
(289, 'ICELANDIC SHEEPDOG'),
(290, 'BEAGLE HARRIER'),
(291, 'EURASIAN'),
(292, 'DOGO ARGENTINO'),
(293, 'AUSTRALIAN KELPIE'),
(294, 'OTTERHOUND'),
(295, 'HARRIER'),
(296, 'COLLIE SMOOTH'),
(297, 'BORDER COLLIE'),
(298, 'ROMAGNA WATER DOG'),
(299, 'GERMAN HOUND'),
(300, 'BLACK AND TAN COONHOUND'),
(301, 'AMERICAN WATER SPANIEL'),
(302, 'IRISH GLEN OF IMAAL TERRIER'),
(303, 'AMERICAN FOXHOUND'),
(304, 'RUSSIAN-EUROPEAN LAIKA'),
(305, 'EAST SIBERIAN LAIKA'),
(306, 'WEST SIBERIAN LAIKA'),
(307, 'AZAWAKH'),
(308, 'DUTCH SMOUSHOND'),
(309, 'SHAR PEI'),
(310, 'PERUVIAN HAIRLESS DOG'),
(311, 'SAARLOOS WOLFHOND'),
(312, 'NOVA SCOTIA DUCK TOLLING RETRIEVER'),
(313, 'DUTCH SCHAPENDOES'),
(314, 'NEDERLANDSE KOOIKERHONDJE'),
(315, 'BROHOLMER'),
(316, 'FRENCH WHITE AND ORANGE HOUND'),
(317, 'KAI'),
(318, 'KISHU'),
(319, 'SHIKOKU'),
(320, 'WIREHAIRED SLOVAKIAN POINTER'),
(321, 'MAJORCA SHEPHERD DOG'),
(322, 'GREAT ANGLO-FRENCH TRICOLOUR HOUND'),
(323, 'GREAT ANGLO-FRENCH WHITE AND BLACK HOUND'),
(324, 'GREAT ANGLO-FRENCH WHITE & ORANGE HOUND'),
(325, 'MEDIUM-SIZED ANGLO-FRENCH HOUND'),
(326, 'SOUTH RUSSIAN SHEPHERD DOG'),
(327, 'RUSSIAN BLACK TERRIER'),
(328, 'CAUCASIAN SHEPHERD DOG'),
(329, 'CANARIAN WARREN HOUND'),
(330, 'IRISH RED AND WHITE SETTER'),
(331, 'ANATOLIAN SHEPHERD DOG'),
(332, 'CZECHOSLOVAKIAN WOLFDOG'),
(334, 'KOREA JINDO DOG'),
(335, 'CENTRAL ASIA SHEPHERD DOG'),
(336, 'SPANISH WATER DOG'),
(337, 'ITALIAN SHORT-HAIRED SEGUGIO'),
(338, 'THAI RIDGEBACK DOG'),
(339, 'PARSON RUSSELL TERRIER'),
(340, 'SAINT MIGUEL CATTLE DOG'),
(341, 'BRAZILIAN TERRIER'),
(342, 'AUSTRALIAN SHEPHERD'),
(343, 'ITALIAN CORSO DOG'),
(344, 'AMERICAN AKITA'),
(345, 'JACK RUSSELL TERRIER'),
(346, 'DOGO CANARIO'),
(347, 'WHITE SWISS SHEPHERD DOG'),
(348, 'TAIWAN DOG'),
(349, 'ROMANIAN MIORITIC SHEPHERD DOG'),
(350, 'ROMANIAN CARPATHIAN SHEPHERD DOG'),
(351, 'AUSTRALIAN STUMPY TAIL CATTLE DOG'),
(352, 'RUSSIAN TOY'),
(353, 'CIMARRÓN URUGUAYO'),
(354, 'POLISH HUNTING DOG'),
(355, 'BOSNIAN AND HERZEGOVINIAN - CROATIAN SHEPHERD DOG'),
(356, 'DANISH-SWEDISH FARMDOG'),
(357, 'SOUTHEASTERN EUROPEAN SHEPHERD'),
(358, 'THAI BANGKAEW DOG'),
(359, 'MINIATURE BULL TERRIER'),
(360, 'LANCASHIRE HEELER');

-- --------------------------------------------------------

--
-- Table structure for table `dog`
--

DROP TABLE IF EXISTS `dog`;
CREATE TABLE `dog` (
  `dog_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` enum('Female','Male') NOT NULL DEFAULT 'Male',
  `color` varchar(50) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `isTrained` tinyint(1) NOT NULL,
  `experienceLevel` int(11) NOT NULL,
  `description` text NOT NULL,
  `size_id` int(11) NOT NULL,
  `shelter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dog`
--

INSERT INTO `dog` (`dog_id`, `name`, `age`, `gender`, `color`, `picture`, `isTrained`, `experienceLevel`, `description`, `size_id`, `shelter_id`) VALUES
(1, 'Jacky', 2, 'Male', 'Brown', '5bedca3c21ede.jpg', 0, 5, 'Something about dog', 4, 2),
(2, 'Kyle', 1, 'Male', 'Black', '5c056d924cbf5.jpg', 0, 5, 'Very funny guy who loves to play with kids', 3, 3),
(3, 'Moly', 3, 'Female', 'Brown', '5c056df0f093c.jpg', 0, 3, 'Really quiet, therefore good for appartments', 4, 3),
(4, 'Sesame', 5, 'Male', 'Golden', '5c056e8870404.jpg', 1, 1, 'Very kind, good for old people', 4, 4),
(5, 'Minou', 2, 'Male', 'grey', '5c056ef75c4d8.jpg', 0, 2, 'Good for kids, doesnt loose fur', 2, 4),
(6, 'Lily', 4, 'Female', 'Golden', '5c056f7c62cd9.jpg', 1, 3, 'Very kind dog, Completely trained.', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `mixbreed`
--

DROP TABLE IF EXISTS `mixbreed`;
CREATE TABLE `mixbreed` (
  `breed_id` int(11) NOT NULL,
  `dog_id` int(11) NOT NULL,
  `mixbreed_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mixbreed`
--

INSERT INTO `mixbreed` (`breed_id`, `dog_id`, `mixbreed_id`) VALUES
(147, 2, 1),
(317, 3, 2),
(36, 4, 3),
(184, 5, 4),
(111, 6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `f_name` varchar(30) NOT NULL,
  `l_name` varchar(30) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `f_name`, `l_name`, `phone`, `picture`, `user_id`) VALUES
(1, 'Irina', 'Kim', '438 938 0109', '5bedca3c21ede.jpg', 1),
(3, 'Irina', 'Kim', '4389380109', '5bf42a460017c.jpg', 4),
(4, 'Mike', 'Pierre', '514 968 5698', '5c056c47ca287.jpg', 6),
(5, 'Jenny', 'Trump', '438 969 8547', '5c056c722b3cb.jpg', 7),
(6, 'Elisabeth', 'Kayne', '514 868 9658', '5c056c9302fb4.jpg', 8);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `request_id` int(11) NOT NULL,
  `dog_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `status` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `dog_id`, `person_id`, `status`) VALUES
(1, 2, 4, 'Accepted'),
(2, 4, 4, 'Pending'),
(3, 1, 5, 'Pending'),
(4, 6, 5, 'Accepted'),
(5, 6, 5, 'Denied'),
(6, 3, 6, 'Pending'),
(7, 5, 6, 'Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `shelter`
--

DROP TABLE IF EXISTS `shelter`;
CREATE TABLE `shelter` (
  `shelter_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `website` varchar(50) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shelter`
--

INSERT INTO `shelter` (`shelter_id`, `name`, `address`, `phone`, `website`, `picture`, `user_id`) VALUES
(2, 'Cute Dogs', '1513 St Jacques', '4389380109', 'http://cutedogs.com', '5bf42b1d2ddfb.jpg', 5),
(3, 'Dog House', '1515 St Catherine', '514 663 2145', 'http://doghouse.com', '5c056cff0f066.png', 9),
(4, 'Doglove', '1893 Jean Brillant', '514 236 9898', 'http://doglove.come', '5c056e3e39120.png', 10),
(5, 'Scoty', '2569 Shevchenko', '514 869 9684', 'http://scoty.com', '5c056f493b603.jpg', 11);

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
CREATE TABLE `size` (
  `size_id` int(11) NOT NULL,
  `size` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`size_id`, `size`) VALUES
(1, 'XS'),
(2, 'S'),
(3, 'M'),
(4, 'L'),
(5, 'XL'),
(6, 'XXL');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(75) NOT NULL,
  `role` enum('Person','Shelter') NOT NULL DEFAULT 'Person'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `role`) VALUES
(1, 'ira1998@rambler.ru', '$2y$10$V5v2LjWv1wvNe1XlR0.Xeu1BtOznfPzuxiTsPeOz5uSNQDxBx6Yj6', 'Person'),
(4, 'iraperson@gmail.com', '$2y$10$bzTg0MojUEYpInJHnFUvgOdkcXTwyh3vObs/BILM2D2GbWV2uJrLm', 'Person'),
(5, 'irashelter@gmail.com', '$2y$10$D2qU1GzahEjkf1xVAaRJCOt38GYFk5q4SNsueUgVmxbjFp.ZP2D0i', 'Shelter'),
(6, 'person1@gmail.com', '$2y$10$jQARyOgkSiMzPhJE/mp60uc6qmBbu8U0EwyCYVgvvenousqseNfJW', 'Person'),
(7, 'person2@gmail.com', '$2y$10$ptO.vdPZurWUlLPgjZ2dvu3ni5WPIRjRrVUHIy9kcy5MH2PCwBXkG', 'Person'),
(8, 'person3@gmail.com', '$2y$10$G/tF1nZdBKuQl8dCYdbOJezKQWyfjeU6mlBYppXLFXQuJqsBoLpce', 'Person'),
(9, 'shelter1@gmail.com', '$2y$10$AA4Gq2Enf9scKQ5aWSBMyuXh9fovloHOb7S/acrl5qajepBlvVT16', 'Shelter'),
(10, 'shelter2@gmail.com', '$2y$10$17y/Dtd4JDOeOmPEwLbzvOyoR58qA3JU5qol4Fw2xkvkHq9YAASTO', 'Shelter'),
(11, 'shelter3@gmail.com', '$2y$10$Z7SnxFoAlQsv3C/nBoahOuhTH3ac/T3v1PVpruH4jTNVysE0tqttK', 'Shelter');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`appointment_id`),
  ADD KEY `FK_SHELT_APP` (`shelter_id`),
  ADD KEY `FK_PERS_APP` (`person_id`),
  ADD KEY `FK_DOG_APP` (`dog_id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`breed_id`);

--
-- Indexes for table `dog`
--
ALTER TABLE `dog`
  ADD PRIMARY KEY (`dog_id`),
  ADD KEY `FK_SIZE_DOG` (`size_id`),
  ADD KEY `FK_SHELT_DOG` (`shelter_id`);

--
-- Indexes for table `mixbreed`
--
ALTER TABLE `mixbreed`
  ADD PRIMARY KEY (`mixbreed_id`),
  ADD KEY `FK_BREED_MIX` (`breed_id`),
  ADD KEY `FK_DOG_MIX` (`dog_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `FK_USER_PERS` (`user_id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `FK_DOG_REQ` (`dog_id`),
  ADD KEY `FK_PERS_REQ` (`person_id`);

--
-- Indexes for table `shelter`
--
ALTER TABLE `shelter`
  ADD PRIMARY KEY (`shelter_id`),
  ADD KEY `FK_USER_SHEL` (`user_id`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `breed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;
--
-- AUTO_INCREMENT for table `dog`
--
ALTER TABLE `dog`
  MODIFY `dog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mixbreed`
--
ALTER TABLE `mixbreed`
  MODIFY `mixbreed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `shelter`
--
ALTER TABLE `shelter`
  MODIFY `shelter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `FK_DOG_APP` FOREIGN KEY (`dog_id`) REFERENCES `dog` (`dog_id`),
  ADD CONSTRAINT `FK_PERS_APP` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`),
  ADD CONSTRAINT `FK_SHELT_APP` FOREIGN KEY (`shelter_id`) REFERENCES `shelter` (`shelter_id`);

--
-- Constraints for table `dog`
--
ALTER TABLE `dog`
  ADD CONSTRAINT `FK_SHELT_DOG` FOREIGN KEY (`shelter_id`) REFERENCES `shelter` (`shelter_id`),
  ADD CONSTRAINT `FK_SIZE_DOG` FOREIGN KEY (`size_id`) REFERENCES `size` (`size_id`);

--
-- Constraints for table `mixbreed`
--
ALTER TABLE `mixbreed`
  ADD CONSTRAINT `FK_BREED_MIX` FOREIGN KEY (`breed_id`) REFERENCES `breed` (`breed_id`),
  ADD CONSTRAINT `FK_DOG_MIX` FOREIGN KEY (`dog_id`) REFERENCES `dog` (`dog_id`);

--
-- Constraints for table `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `FK_USER_PERS` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `request`
--
ALTER TABLE `request`
  ADD CONSTRAINT `FK_DOG_REQ` FOREIGN KEY (`dog_id`) REFERENCES `dog` (`dog_id`),
  ADD CONSTRAINT `FK_PERS_REQ` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`);

--
-- Constraints for table `shelter`
--
ALTER TABLE `shelter`
  ADD CONSTRAINT `FK_USER_SHEL` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
